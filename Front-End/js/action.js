
// Hàm lấy thông tin giỏ hàng
function getCartItems() {
  let vCartItems = localStorage.getItem("cartItems");
  if (vCartItems) {
    vCartItems = JSON.parse("cartItems");
  }
  else {
    vCartItems = [];
  }
  return vCartItems;
}

// Hàm load từng sản phẩm ra trang chủ
function loadProduct(paramTag, paramProduct) {
  let vProduct = `<div class="col-xl-3 col-lg-4 col-sm-6">
                <div id="`+ paramProduct.id + `" class="product text-center">
                  <div class="position-relative mb-3">
                    <a class="d-block" href="product-detail.html?name=`+ paramProduct.productName + `"><img class="img-fluid w-100"
                        src="assets/image/product/`+ paramProduct.productImages[0].imgLink + `" alt="..."></a>
                    <div class="product-overlay">
                      <ul class="mb-0 list-inline">
                        <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark btn-add-to-wishlist"><i
                              class="far fa-heart"></i></a></li>
                        <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark btn-add-to-cart"
                          href="product-detail.html?name=`+ paramProduct.productName + `" >Add to cart</a></li>
                        <li class="list-inline-item me-0 view-product"><a class="btn btn-sm btn-outline-dark"><i class="fas fa-expand"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <h6> <a class="reset-anchor product-name" href="product-detail.html?name=`+ paramProduct.productName + `">` + limitProductName(paramProduct.productName, 32) + `</a></h6>
                  <p class=" text-muted">`+ formatPriceToVND(paramProduct.minPrice) + `</p>
                </div>
              </div>`
  paramTag.append(vProduct);
}


// Hàm load sản phẩm ra danh mục yêu thích
function loadWishlistProduct(paramTag, paramProduct) {
  let vProduct = `<div class="col-xl-3 col-lg-4 col-sm-6">
                <div id="`+ paramProduct.id + `" class="product text-center">
                  <div class="position-relative mb-3">
                    <a class="d-block" href="product-detail.html?name=`+ paramProduct.productName + `"><img class="img-fluid w-100"
                        src="assets/image/product/`+ paramProduct.productImages[0].imgLink + `" alt="..."></a>
                        <button class="btn btn-dark btn-remove-item">x</button>
                    <div class="product-overlay">
                      <ul class="mb-0 list-inline">
                        <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark btn-add-to-wishlist"><i
                              class="far fa-heart"></i></a></li>
                        <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark btn-add-to-cart"
                          href="product-detail.html?name=`+ paramProduct.productName + `" >Add to cart</a></li>
                        <li class="list-inline-item me-0 view-product"><a class="btn btn-sm btn-outline-dark"><i class="fas fa-expand"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <h6> <a class="reset-anchor product-name" href="product-detail.html?name=`+ paramProduct.productName + `">` + limitProductName(paramProduct.productName, 32) + `</a></h6>
                  <p class=" text-muted">`+ formatPriceToVND(paramProduct.minPrice) + `</p>
                </div>
              </div>`
  paramTag.append(vProduct);
}

// Hàm load tất cả sản phẩm ra form ở trang tất cả sản phẩm
function loadAllProduct(paramTag, paramProduct) {
  let vProduct = `<div class="col-xl-4 col-lg-6 col-sm-6">
  <div id="`+ paramProduct.id + `" class="product text-center">
    <div class="position-relative mb-3">
      <a class="d-block" href="product-detail.html?name=`+ paramProduct.productName + `"><img class="img-fluid w-100"
      src="assets/image/product/`+ paramProduct.productImages[0].imgLink + `" alt="..."></a>
      <div class="product-overlay">
        <ul class="mb-0 list-inline">
          <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-outline-dark btn-add-to-wishlist" href="#!"><i
                class="far fa-heart"></i></a></li>
          <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="product-detail.html?name=`+ paramProduct.productName +`">Add to
              cart</a></li>
          <li class="list-inline-item me-0 view-product"><a class="btn btn-sm btn-outline-dark" ><i class="fas fa-expand"></i></a></li>
        </ul>
      </div>
    </div>
    <h6> <a class="reset-anchor product-name" href="product-detail.html?name=`+ paramProduct.productName + `">` + limitProductName(paramProduct.productName, 32) + `</a></h6>
    <p class="text-muted">`+ formatPriceToVND(paramProduct.minPrice) + `</p>
  </div>
</div>`
  paramTag.append(vProduct);
}

// Hàm load tất cả sản phẩm phổ biến trả về từ api vào form trang chủ
function loadListProduct(paramTag, paramListProduct) {
  for (let bI = 0; bI < paramListProduct.length; bI++) {
    loadProduct(paramTag, paramListProduct[bI]);
  }
}


// Hàm load tất cả sản phẩm phổ biến trả về từ api vào form trang tất cả sản phẩm
function loadListAllProduct(paramTag, paramListProduct) {
  for (let bI = 0; bI < paramListProduct.length; bI++) {
    loadAllProduct(paramTag, paramListProduct[bI]);
  }
}




// Hàm thay đổi giá trị tiền tệ
function formatPriceToVND(paramPrice) {
  let vStringPrice = paramPrice + "";
  let vResult = "";
  let vCount = 0;
  for (let bI = vStringPrice.length - 1; bI >= 0; bI--) {
    vCount++;
    vResult += vStringPrice[bI];
    if (vCount == 3 && bI != 0) {
      vCount = 0;
      vResult += ".";
    }
  }
  // Đảo ngược lại chuỗi
  vResult = vResult.split('').reverse().join('');
  vResult += " VNĐ"
  return vResult;
}

// Đổi từ VNĐ sang kiểu số
function formatVNDToNumber(paramVND) {
  return parseFloat(paramVND.replace(/[^\d]/g, ''));
}

// Hàm giới hạn kí tự hiển thị
function limitProductName(name, maxLength) {
  if (name.length > maxLength) {
    return name.substring(0, maxLength) + "...";
  }
  return name;
}


// Hàm gọi API để load thông tin sản phẩm
function callApiToLoadProductData(paramProductName) {
  $.ajax({
    type: "GET",
    url: gBASE_URL_PRODUCT + "detail?name=" + paramProductName,
    success: function (paramRes) {
      gProductId = paramRes.id;
      loadAverageRatingAndNumberRating(paramRes.ratings);
      loadProductData(paramRes);
    },
    error: function (paramError) {
      console.log(paramError.status);
    }
  });
}

 // Hàm load dữ liệu sao trung bình và số lượng người đánh giá sản phẩm
 function loadAverageRatingAndNumberRating(paramListRating) {
  let vAverageStar = 0;
  

  for(let bI = 0; bI< paramListRating.length; bI++) {
    vAverageStar += paramListRating[bI].ratingPoint;
  }

  if(paramListRating.length > 0) {
    vAverageStar = parseFloat(vAverageStar/paramListRating.length);
    // Set giá trị cho trung bình sao 
    $('#average-rating').rating('rate', vAverageStar);
    // Set số người đánh giá
    $("#number-rating").html("(" + paramListRating.length + ")");
  }
  else {
    $("#number-rating").html("(0)");
    $('#average-rating').rating('rate', 0);
  }
  
}

// Hàm load dữ liệu trả về từ APi vào trang chi tiết sản phẩm
function loadProductData(paramData) {
  // Thay đổi title link trên dòng điều hướng
  $(".title-link").html(paramData.productName);

  // Hiển thị tên sản phẩm
  let vH2ProductTitle = $(".product-title").find("h2");
  vH2ProductTitle.html(paramData.productName);

  // Hiển thị mã sản phẩm
  let vSpanProductCode = $(".product-code").find("span");
  vSpanProductCode.html(paramData.productCode);

  // Hiển thị hình ảnh chính sản phẩm
  let vLightBoxImg = $(".lightbox").find("img");
  vLightBoxImg.attr("src", 'assets/image/product/' + paramData.productImages[0].imgLink);

  // Hiển thị list hình ảnh phụ kế bên sản phẩm
  let vListVerticalGalleryImg = $(".vertical-galery").find("img");
  let vListEcommerceGallery = $(".ecommerce-gallery").find("img");
  let vHorizontalGallery = $(".horizontal-galery").find("img")
  let vNumberImg = paramData.productImages.length;
  if(vNumberImg > 4) {
    vNumberImg = 4
  }
  for (let bI = 0; bI < vNumberImg; bI++) {
    // Ảnh hiển thị 
    $(vListVerticalGalleryImg[bI]).attr("src", 'assets/image/product/' + paramData.productImages[bI].imgLink);
    $(vListVerticalGalleryImg[bI]).attr("data-mdb-img", 'assets/image/product/' + paramData.productImages[bI].imgLink);

    // Ảnh để chuyển qua ảnh chính khi click vào
    $(vListEcommerceGallery[bI]).attr("src", 'assets/image/product/' + paramData.productImages[bI].imgLink);
    $(vListEcommerceGallery[bI]).attr("data-mdb-img", 'assets/image/product/' + paramData.productImages[bI].imgLink);

    $(vHorizontalGallery[bI]).attr("src", 'assets/image/product/' + paramData.productImages[bI].imgLink);
    $(vHorizontalGallery[bI]).attr("data-mdb-img", 'assets/image/product/' + paramData.productImages[bI].imgLink);

  }

  // Hàm để load ra danh sách size của sản phẩm
  // Trước hết là phải làm sạch những kích cỡ cũ đã
  $(".product-size").html("");
  for (let bI = 0; bI < paramData.productSizes.length; bI++) {
    loadSizeForProduct(paramData.productSizes[bI], bI + 1);
  }

  // Điền giá sản phẩm bằng kích thước đầu tiên dùng hàm format price để đưa về giá trị tiền VNĐ
  let vH4ProductPrice = $(".product-price").find("h4");
  let vPriceFormatToVND = formatPriceToVND(paramData.productSizes[0].price);
  vH4ProductPrice.html(vPriceFormatToVND);

}


// Hàm load size cho sản phẩm
function loadSizeForProduct(paramSize, numberOption) {
  let vDivProductSize = $(".product-size");
  let vSizeDisplay = `<input size-id="` + paramSize.id + `" price="` + paramSize.price + `" product-sold="` + paramSize.productSold + `" type="radio" class="btn-check mt-2" name="options" id="option` + numberOption + `" autocomplete="off">
              <label class="btn btn-outline-dark" for="option`+ numberOption + `">` + paramSize.size + `</label>`;

  let vSizeDisplayChecked = `<input size-id="` + paramSize.id + `" price="` + paramSize.price + `" product-sold="` + paramSize.productSold + `" type="radio" class="btn-check mt-2" name="options" id="option` + numberOption + `" autocomplete="off" checked>
              <label class="btn btn-outline-dark" for="option`+ numberOption + `">` + paramSize.size + `</label>`;
  if (numberOption == 1) {
    vDivProductSize.append(vSizeDisplayChecked);
  }
  else {
    vDivProductSize.append(vSizeDisplay);
  }
}


// Hàm xử lý sự kiện nhấn nút xem trước sản phẩm
function onBtnViewProductClick(paramBtn) {
  let vAProductName = $(paramBtn).closest(".product").find(".product-name");
  let vLinkToProductDetail = vAProductName.attr("href");
  let vUrlParam = new URLSearchParams(vLinkToProductDetail);
  let vNamePositon = vLinkToProductDetail.indexOf("name=");
  let vNameProduct = vLinkToProductDetail.substr(vNamePositon).substr(5);

  callApiToLoadProductData(vNameProduct);
  $("#modal-view-product").modal("show");
}

// Hàm xử lý sự kiện nhấn nút thêm vào wishlist ở trang chủ và trang tất cả sản phẩm;
function onBtnAddToWishListClick(paramBtn) {
  let vWishListItem = localStorage.getItem('wishListItem');
  let vBoxProduct = $(paramBtn).closest(".product");
  let vAProductImgLink = vBoxProduct.find(".position-relative a");
  let vHref = vAProductImgLink.attr("href");
  let vPositionProductName = vHref.indexOf("=");
  let vProductName = vHref.substring(vPositionProductName + 1);
  if (vWishListItem) {
    vWishListItem = JSON.parse(vWishListItem);
  } else {
    vWishListItem = [];
  }
  if (vWishListItem.includes(vProductName)) {
    alertWarning("Sản phẩm đã có trong danh mục yêu thích!");
  }
  else {
    alertSuccess("Sản phẩm đã được thêm vào danh mục yêu thích!");
    vWishListItem.push(vProductName);
  }
  localStorage.setItem('wishListItem', JSON.stringify(vWishListItem));
  displayNumberWishlist();
}

// Hàm xử lý sự kiện nhấn nút thêm vào wishlist ở phần xem chi tiết sản phẩm;
function onBtnAddToWishListClickOnProductDetail() {
  let vWishListItem = localStorage.getItem('wishListItem');
  let vProductName = $(".product-title h2").html();
  if (vWishListItem) {
    vWishListItem = JSON.parse(vWishListItem);
  } else {
    vWishListItem = [];
  }
  if (vWishListItem.includes(vProductName)) {
    alertWarning("Sản phẩm đã có trong danh mục yêu thích!");
  }
  else {
    alertSuccess("Sản phẩm đã được thêm vào danh mục yêu thích!");
    vWishListItem.push(vProductName);
  }
  localStorage.setItem('wishListItem', JSON.stringify(vWishListItem));
  displayNumberWishlist();
}

// Hàm hiển thị số lượng sản phẩm yêu thích
function displayNumberWishlist() {
  let vWishListItem = localStorage.getItem("wishListItem");
  if(vWishListItem) {
    vWishListItem = JSON.parse(vWishListItem)
  }
  else {
    vWishListItem = [];
  }
  let vNumberWishList = "(" +  vWishListItem.length + ")";
  let vSpanNumberWishlist = $(".number-wishlist");
  for(let bI = 0; bI < vSpanNumberWishlist.length; bI++) {
    vSpanNumberWishlist.html(vNumberWishList);
  }

}

// Hàm xử lý sự kiện nhấn nút add to cart;
function onBtnAddToCartClick() {
  let vRadioSelected = $(".product-size").find("input[name=options]:checked");
  let vSizeId = vRadioSelected.attr("size-id");
  let cartItems = localStorage.getItem('cartItems');
  let vQuantityOrder = $("#inp-qty").val();
  if (cartItems) {
    cartItems = JSON.parse(cartItems);
  } else {
    cartItems = [];
  }
  for (let bI = 0; bI < vQuantityOrder; bI++) {
    cartItems.push(vSizeId);
  }
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
  displayNumberOrder();
  alertSuccess("Sản phẩm đã được thêm vào giỏ hàng!");
  // Phải reset size và số lượng về 1
  $("#inp-qty").val(1);
  $("#modal-view-product").modal("hide");
}


function onBtnCartClick() {
  let cartItems = localStorage.getItem("cartItems");
  if (cartItems) {
    cartItems = JSON.parse(cartItems);
  }
  else {
    cartItems = [];
  }
  return cartItems;
}

// Hàm hiển thị số lượng đặt hàng từ thông tin lấy từ localStorage
function displayNumberOrder() {
  let cartItems = localStorage.getItem("cartItems");
  if (cartItems) {
    cartItems = JSON.parse(cartItems);
  }
  else {
    cartItems = [];
  }
  var vNumber = "(" + cartItems.length + ")";
  var vListNumber = $(".number-order");
  for (var i = 0; i < vListNumber.length; i++) {
    $(vListNumber[i]).html(vNumber);
  }
}



// Thay đổi kiểu lưu sản cookie từ chuỗi sang đối tượng theo từng số lượng 
function changeTypeOrderProduct(paramArrProduct) {
  let vResult = {};
  $.each(paramArrProduct, function (index, element) {
    if (vResult[element]) {
      vResult[element]++;
    }
    else {
      vResult[element] = 1;
    }
  })
  return vResult;
}


//Xử lý object trả về khi login thành công
function responseHandler(data) {
  //Lưu token vào cookie trong 1 ngày
  setCookie("token", data, 1);
  setTimeout(function () {
    // Navigate to index.html
    window.location.href = "index.html";
  }, 1000);
}

//Hàm setCookie đã giới thiệu ở bài trước
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm get Cookie đã giới thiệu ở bài trước
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

// Hàm gọi API để hiển thông tin tên đăng nhập kèm lời chào
function callApiToHelloUserByToken() {
  //Khai báo xác thực ở headers
  var vHeaders = {
    Authorization: "Token " + gTOKEN
  };

  $.ajax({
    type: "GET",
    url: gBASE_URL + "hello1",
    headers: vHeaders,
    success: function (paramRes) {
      displayHelloUser(paramRes);
    },
    error: function (paramError) {
      console.log(paramError.status);
    }
  });
}

// Hàm hiển thị thông báo xin chào người dùng và hiển thị các ô thao tác xem thông tin, đăng nhập, đăng xuất
function displayHelloUser(paramRes) {
  let vAnnouncementHello = $(".announcement-hello");
  vAnnouncementHello.html("Hi " + paramRes.username);
  vAnnouncementHello.show();
  let vAnnouncementLogin = $(".announcement-login");
  vAnnouncementLogin.hide();

  let vInfoLogin = $(".info-login");
  vInfoLogin.show();

  let vInfoLoginMobile = $(".info-login-mobile");
  let vSpanUserMobile = vInfoLoginMobile.find(".hi-user");
  vSpanUserMobile.html(`<i class="fa-regular fa-user"></i> Hi ` + paramRes.username + ` <i class="fas fa-sort-down mb-1"></i>`);

  let vLinkDashBoard = $(".admin-page");
  if (paramRes.authorities[0] === "ROLE_ADMIN" || paramRes.authorities[0] === "ROLE_MANAGER") {
    vLinkDashBoard.show();
  }
  vInfoLoginMobile.show();

}

// Hàm gọi API để check quyền truy cập trang admin và hiển thị xin chào
function callApiToHelloAndAuthenAdminPage() {
  //Khai báo xác thực ở headers
  var vHeaders = {
    Authorization: "Token " + gTOKEN
  };

  $.ajax({
    type: "GET",
    url: gBASE_URL + "hello1",
    headers: vHeaders,
    success: function (paramRes) {
      helloAdminAndAuthentic(paramRes);
    },
    error: function (paramError) {
      alertSuccess("Có lỗi xảy ra vui lòng thử lại sau!");
      window.location.href = "../login.html";
      console.log(paramError.status);
    }
  });
}

// Hiển thị xin chào và xác thực ở trang admin, nếu k thuộc role manage hoặc role admin thì trở về trang chủ trang khách hàng
function helloAdminAndAuthentic(paramModata) {
  let vInfoAdmin = $(".info").find("a");
  if (paramModata.authorities[0] == "ROLE_ADMIN") {
    vInfoAdmin.html("Admin " + paramModata.username);
  }
  else if (paramModata.authorities[0] == "ROLE_MANAGER") {
    vInfoAdmin.html("Manage " + paramModata.username);
    let vUserManagerLink = $(".user-manager");
    vUserManagerLink.remove();
  }
  else {
    window.location.href = "../login.html";
  }
}
// Hàm hiển thông báo  thành công
function alertSuccess(paramNotification) {
  gToast.fire({
    icon: 'success',
    title: paramNotification
  });
}

// Hàm thông báo lỗi validate theo toast
function alertWarning(paramTitile) {
  gToast.fire({
    icon: 'warning',
    title: "" + paramTitile
  });
}

// Hàm hiển thông báo có lỗi trong quá trình đăng ký
function alertError(paramTitile) {
  gToast.fire({
    icon: 'error',
    title: paramTitile
  });
}
