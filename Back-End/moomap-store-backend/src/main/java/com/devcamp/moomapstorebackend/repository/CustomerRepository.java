// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Customer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
   Customer findByPhoneNumber(String phoneNumber);

   // Đếm số lượng khách hàng bởi xếp hạng
   @Query(value = "SELECT r.rank, COUNT(c.rank) AS count FROM " + 
         "( SELECT 'Platinum' AS rank UNION ALL SELECT 'Gold' UNION ALL SELECT 'Silver' UNION ALL SELECT 'VIP' ) AS r  LEFT JOIN " +
         "( SELECT CASE WHEN COALESCE(SUM(o.ammount), 0) >= 5000000 THEN 'Platinum' " +
         "WHEN COALESCE(SUM(o.ammount), 0) >= 2000000 THEN 'Gold' " +
         "WHEN COALESCE(SUM(o.ammount), 0) >= 1000000 THEN 'Silver' " +
         "ELSE 'VIP' END AS rank FROM customers c LEFT JOIN "+
         "orders o ON c.id = o.customer_id GROUP BY c.id ) AS c ON r.rank = c.rank GROUP BY r.rank ORDER BY CASE r.rank "+
         "WHEN 'Platinum' THEN 1 WHEN 'Gold' THEN 2 WHEN 'Silver' THEN 3 ELSE 4 END", nativeQuery = true)
   List<Object> getNumberOfCustomerRank();

   // Lấy ra danh sách khách hàng đáp ứng các điều kiện tên, xếp hạng, kiểu sắp xếp
   @Query(value = "SELECT * FROM customers WHERE CONCAT(customers.first_name, ' ', customers.last_name) LIKE %:name% " +
                  "AND phone_number LIKE %:phone%" +
                  "AND rank LIKE %:rank% ORDER BY " +
                  "CASE WHEN :type = 'ASC' THEN customers.number_orders END ASC, " + 
                  "CASE WHEN :type = 'DESC' THEN customers.number_orders END DESC", nativeQuery = true)
   List<Customer> filterCustomer(@Param("name") String name,@Param("type") String type, @Param("rank") String rank, @Param("phone") String phone);

   // Lấy ra danh sách khách hàng theo hạng
   @Query(value = "SELECT * FROM customers WHERE rank LIKE %:rank%", nativeQuery = true)
   List<Customer> findAllCustomersByRank(@Param("rank") String rank);
}


