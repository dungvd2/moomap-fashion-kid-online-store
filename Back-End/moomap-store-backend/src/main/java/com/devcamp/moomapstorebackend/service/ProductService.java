// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.FilterCondition;
import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.models.ProductImage;
import com.devcamp.moomapstorebackend.models.ProductLine;
import com.devcamp.moomapstorebackend.models.ProductSize;
import com.devcamp.moomapstorebackend.repository.ProductImageRepository;
import com.devcamp.moomapstorebackend.repository.ProductLineRepository;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import com.devcamp.moomapstorebackend.repository.ProductSizeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
   @Autowired
   ProductRepository iProduct;
   
   @Autowired
   ProductLineRepository iProductLine;
   
   @Autowired
   ProductImageRepository iProductImage;
   
   @Autowired
   ProductSizeRepository iProductSize;

   @Autowired
   ProductSizeService productSizeService;

   public ProductService() {
   }

   public List<Product> getAllProduct() {
      return this.iProduct.findAll();
   }

   public Product getProductById(Integer productId) {
      return (Product)this.iProduct.findById(productId).get();
   }

   public List<Product> getAllProductsOfProductLine(Integer productLineId) {
      ProductLine productLine = (ProductLine)this.iProductLine.findById(productLineId).get();
      return productLine.getProducts();
   }

   
   public Product createProduct(Product pProduct, Integer productLineId) {
      Product product = new Product();
      ProductLine productLine = (ProductLine)this.iProductLine.findById(productLineId).get();
      product.setProductLine(productLine);
      product.setProductCode(pProduct.getProductCode());
      product.setProductName(pProduct.getProductName());
      product.setProductDescription(pProduct.getProductDescription());
      product.setUserFor(pProduct.getUserFor());
      List<ProductSize> productSizes = new ArrayList<>();
      Iterator var7 = pProduct.getProductSizes().iterator();

      while(var7.hasNext()) {
         ProductSize productSize = (ProductSize)var7.next();
         ProductSize pSize = new ProductSize();
         pSize.setSize(productSize.getSize());
         pSize.setPrice(productSize.getPrice());
         pSize.setQuantityInStock(productSize.getQuantityInStock());
         pSize.setProduct(product);
         pSize.setProductSold(0);
         pSize.setProductName(product.getProductName());
         productSizes.add(pSize);
      }

      product.setProductSizes(productSizes);
      List<ProductImage> productImages = new ArrayList<>();
      Iterator var13 = pProduct.getProductImages().iterator();

      while(var13.hasNext()) {
         ProductImage productImage = (ProductImage)var13.next();
         ProductImage pImage = new ProductImage();
         pImage.setImgLink(productImage.getImgLink());
         pImage.setProduct(product);
         productImages.add(pImage);
      }

      product.setProductImages(productImages);
      Product productCreated = (Product)this.iProduct.save(product);
      return productCreated;
   }

   public Product updateProduct(Product pProduct, Integer productLineId, Integer productId) {
      ProductLine productLine = (ProductLine)this.iProductLine.findById(productLineId).get();
      Product product = (Product)this.iProduct.findById(productId).get();
      product.removeProductSize(product.getProductSizes());
      product.setProductLine(productLine);
      product.setProductCode(pProduct.getProductCode());
      product.setProductDescription(pProduct.getProductDescription());
      product.setUserFor(pProduct.getUserFor());
      product.setProductName(pProduct.getProductName());
      product.removeProductImages(product.getProductImages());
      List<ProductSize> productSizes = new ArrayList<>();
      Iterator var8 = pProduct.getProductSizes().iterator();

      while(var8.hasNext()) {
         ProductSize productSize = (ProductSize)var8.next();

         Optional<ProductSize> sizeContainer = iProductSize.findById(productSize.getId());
         if(sizeContainer.isPresent()) {
            ProductSize size = sizeContainer.get();
            size.setPrice(productSize.getPrice());
            size.setQuantityInStock(productSize.getQuantityInStock());
            size.setSize(productSize.getSize());
            size.setProduct(product);
            size.setProductName(product.getProductName());
            iProductSize.save(size);
            productSizes.add(size);
         }
         else {
            ProductSize size = new ProductSize();
            size.setPrice(productSize.getPrice());
            size.setProductSold(0);
            size.setQuantityInStock(productSize.getQuantityInStock());
            size.setSize(productSize.getSize());
            size.setProductName(product.getProductName());
            size.setProduct(product);
            iProductSize.save(size);
            productSizes.add(size);
         }
      }

      product.setProductSizes(productSizes);

      List<ProductImage> productImages = new ArrayList<>();
      Iterator var14 = pProduct.getProductImages().iterator();

      while(var14.hasNext()) {
         ProductImage productImage = (ProductImage)var14.next();
         ProductImage pImage = new ProductImage();
         pImage.setImgLink(productImage.getImgLink());
         pImage.setProduct(product);
         productImages.add(pImage);
      }

      product.setProductImages(productImages);
      this.iProductImage.deleteImagesWithNullProductId();
      Product productUpdated = (Product)this.iProduct.save(product);
      return productUpdated;
   }

   public void deleteProduct(Integer productId) {
      List<ProductSize> listProductSizes = iProductSize.findAllByProductId(productId);
      for(int i = 0; i < listProductSizes.size(); i++) {
         ProductSize pSize = listProductSizes.get(i);
         pSize.setProduct(null);
         iProductSize.save(pSize);
      }
      this.iProduct.deleteById(productId);
   }

   // Hàm phân trang cho product
   public Page<Product> getProductByPage(String code, String name, int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Product> products = iProduct.getListProductByProductNameAndProductCode(name, code);
      int start = page * size;
      int end = Math.min(start + size, products.size());
      Page<Product> productNavigate = new PageImpl<>(products.subList(start, end), pageable, products.size());
      return productNavigate;
   }

   // Hàm phân trang cho product sau khi lọc
   public Page<Product> filterProductByPage(FilterCondition condition, int page, int size) {
      List<Product> products = this.filterProducts(condition);
      Pageable pageable = PageRequest.of(page, size);
      int start = page * size;
      int end = Math.min(start + size, products.size());
      Page<Product> productNavigate = new PageImpl<>(products.subList(start, end), pageable, products.size());
      return productNavigate;
   }

   // Hàm lọc sản phẩm ra theo các tiêu chí
   public List<Product> filterProducts(FilterCondition condition) {
      List<String> useFor = condition.getGenders();
      List<String> productLines = condition.getLines();
      List<Integer> prices = condition.getPrices();
      String type = condition.getType();
      List<Product> listProducts = iProduct.findAll();
      if(!useFor.isEmpty())  {
         List<Product> filterProducts = new ArrayList<>();
         for (String useForChar : useFor) {
         List<Product> filteredList = listProducts.stream()
                 .filter(product -> product.getUserFor() == useForChar.charAt(0))
                 .collect(Collectors.toList());
         filterProducts.addAll(filteredList);
         }
         listProducts = filterProducts;
      }
      
      if(!productLines.isEmpty()) {
         List<Product> filterProducts = new ArrayList<>();
         for(String productLine : productLines) {
         List<Product> filteredList = listProducts.stream()
                           .filter(product -> product.getProductLine().getProductLine().contains(productLine))
                           .collect(Collectors.toList());
         filterProducts.addAll(filteredList);
         }
         listProducts = filterProducts;
      }
      // Hàm để lọc dữ liệu theo key
      // 1 nhỏ hơn 50k, 2 50 đến 100k, 3 100-200k, 4 200-300k, 5 trên 300k
      if(!prices.isEmpty()) {
         List<Product> filterProducts = new ArrayList<>();
         for(Integer key : prices) {
            if(key == 1) {
               List<Product> filteredList = listProducts.stream()
                        .filter(product -> product.getMinPrice() < 50000)
                        .collect(Collectors.toList());
               filterProducts.addAll(filteredList);
            }
            else if(key == 2) {
               List<Product> filteredList = listProducts.stream()
                        .filter(product -> product.getMinPrice() >= 50000 && product.getMinPrice() < 100000)
                        .collect(Collectors.toList());
               filterProducts.addAll(filteredList);
            }
            else if(key == 3) {
               List<Product> filteredList = listProducts.stream()
                        .filter(product -> product.getMinPrice() >= 100000 && product.getMinPrice() < 200000)
                        .collect(Collectors.toList());
               filterProducts.addAll(filteredList);
            }
            else if(key == 4) {
               List<Product> filteredList = listProducts.stream()
                        .filter(product -> product.getMinPrice() >= 200000 && product.getMinPrice() < 300000)
                        .collect(Collectors.toList());
               filterProducts.addAll(filteredList);
            }
            else {
               List<Product> filteredList = listProducts.stream()
                        .filter(product -> product.getMinPrice() >= 300000)
                        .collect(Collectors.toList());
               filterProducts.addAll(filteredList);
            }
            
         }
         listProducts = filterProducts;
      }
      List<Product> savedDefault = listProducts;
      // Nếu type khác mặc định thì sắp xếp theo các kiểu yêu cầu
      if(type.equals("default"))  {
         listProducts = savedDefault;
      }
      // Dùng colections.sort để sắp xếp list, .reversed() để đảo ngược lại
      else if(type.equals("best-seller")) {
         Collections.sort(listProducts, Comparator.comparingInt(Product:: getAllProductSold));
      }
      else if(type.equals("asc")) {
         Collections.sort(listProducts, Comparator.comparingInt(Product:: getMinPrice));
      }
      else if(type.equals("desc")) {
         Collections.sort(listProducts, Comparator.comparingInt(Product:: getMinPrice).reversed());
      }
      return listProducts;
   }
}
