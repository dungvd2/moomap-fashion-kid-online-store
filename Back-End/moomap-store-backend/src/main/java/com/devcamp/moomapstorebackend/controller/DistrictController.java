// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.District;
import com.devcamp.moomapstorebackend.repository.DistrictRepository;
import com.devcamp.moomapstorebackend.service.DistrictSerivce;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class DistrictController {
   @Autowired
   DistrictSerivce districtService;
   @Autowired
   DistrictRepository iDistrict;

   public DistrictController() {
   }

   @GetMapping({"/districts"})
   public ResponseEntity<List<District>> findAll() {
      try {
         List<District> districts = this.districtService.getAll();
         return new ResponseEntity<>(districts, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/provinces/{id}/districts"})
   public ResponseEntity<List<District>> getListDistrictsByProvinceId(@PathVariable("id") int id) {
      try {
         return new ResponseEntity<>(this.iDistrict.getListDistrictsByProvinceId(id), HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/districts/{id}"})
   public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
      try {
         return new ResponseEntity<>((District)this.iDistrict.findById(id).get(), HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/provinces/{provinceId}/districts"})
   public ResponseEntity<District> createDistrict(@RequestBody District district, @PathVariable("provinceId") int provinceId) {
      try {
         District createdDistrict = this.districtService.createDistrict(district, provinceId);
         return new ResponseEntity<>(createdDistrict, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping({"/provinces/{provinceId}/districts/{districtId}"})
   public ResponseEntity<District> updateDistrict(@RequestBody District district, @PathVariable("provinceId") int provinceId, @PathVariable("districtId") int districtId) {
      try {
         District updatedDistrict = this.districtService.updateDistrict(district, provinceId, districtId);
         return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
      } catch (Exception var5) {
         System.out.println(var5);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"/districts/{id}"})
   public ResponseEntity<District> deleteDistrict(@PathVariable("id") int id) {
      try {
         this.districtService.deleteDistrict(id);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
