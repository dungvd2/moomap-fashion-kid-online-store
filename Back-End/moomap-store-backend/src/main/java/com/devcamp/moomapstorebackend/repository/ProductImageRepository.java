// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.ProductImage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ProductImageRepository extends JpaRepository<ProductImage, Integer> {
   List<ProductImage> findAllByProductId(Integer productId);

   @Transactional
   @Modifying
   @Query(
      value = "DELETE FROM `product_images` WHERE product_id IS NULL",
      nativeQuery = true
   )
   void deleteImagesWithNullProductId();
}
