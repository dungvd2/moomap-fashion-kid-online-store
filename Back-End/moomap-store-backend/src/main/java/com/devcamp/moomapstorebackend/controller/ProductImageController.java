// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.ProductImage;
import com.devcamp.moomapstorebackend.service.ProductImageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProductImageController {
   @Autowired
   ProductImageService productImageService;

   public ProductImageController() {
   }

   @GetMapping({"products/{productId}/images"})
   public ResponseEntity<List<ProductImage>> getAllImageOfProduct(@PathVariable("productId") Integer productId) {
      try {
         List<ProductImage> listImage = this.productImageService.getAllImageOfProduct(productId);
         return new ResponseEntity<>(listImage, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"products/{productId}/images/"})
   public ResponseEntity<ProductImage> createProductImage(@PathVariable("productId") Integer productId, @RequestBody ProductImage pProductImage) {
      try {
         ProductImage productImage = this.productImageService.createNewProductImage(productId, pProductImage);
         return new ResponseEntity<>(productImage, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"images/{imageId}"})
   public ResponseEntity<ProductImage> deleteProductImage(@PathVariable("imageId") Integer imageId) {
      try {
         this.productImageService.deleteProductImage(imageId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
