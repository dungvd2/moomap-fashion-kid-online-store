// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(
   name = "product_sizes"
)
public class ProductSize {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   
   @Column(name = "product_size") 
   private @NotEmpty(message = "Input product Size") String size;
   
   private @NotNull(message = "Input size price") Integer price;
   
   @Column(name = "quantity_in_stock")
   private @NotNull(message = "Input quantity in stock") Integer quantityInStock;
   
   @Column(name = "product_sold")
   public Integer productSold;
   


   @ManyToOne
   @JoinColumn(name = "product_id" , nullable = true)
   @JsonIgnore
   private Product product;
   
   @Transient
   private String firstImg;

   @Transient
   private Integer productIdd;

   @Column(name = "product_name")
   private String productName;
   
   @OneToMany(cascade = CascadeType.ALL,
      mappedBy = "productSize",
      fetch = FetchType.LAZY
   )
   private List<OrderDetail> orderDetails;

   public ProductSize() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getSize() {
      return this.size;
   }

   public void setSize(String size) {
      this.size = size;
   }

   public int getPrice() {
      return this.price;
   }

   public void setPrice(int price) {
      this.price = price;
   }

   public Product getProduct() {
      return this.product;
   }

   public void setProduct(Product product) {
      this.product = product;
   }

   public int getQuantityInStock() {
      return this.quantityInStock;
   }

   public void setQuantityInStock(int quantityInStock) {
      this.quantityInStock = quantityInStock;
   }

   public Integer getProductSold() {
      return this.productSold;
   }

   public void setProductSold(Integer productSold) {
      this.productSold = productSold;
   }

   public List<OrderDetail> getOrderDetails() {
      return this.orderDetails;
   }

   public void setOrderDetails(List<OrderDetail> orderDetails) {
      this.orderDetails = orderDetails;
   }

   public void setPrice(Integer price) {
      this.price = price;
   }

   public void setQuantityInStock(Integer quantityInStock) {
      this.quantityInStock = quantityInStock;
   }

   public String getProductName() {
      return productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public String getFirstImg() {
      return this.getProduct().getProductImages().get(0).getImgLink();
   }

   public Integer getProductIdd() {
      return this.getProduct().getId();
   }
}
