// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Province;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
}
