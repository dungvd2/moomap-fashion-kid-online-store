// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.OrderDetail;
import com.devcamp.moomapstorebackend.service.OrderDetailService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class OrderDetailController {
   @Autowired
   OrderDetailService orderDetailService;

   public OrderDetailController() {
   }

   @GetMapping({"/order-details"})
   public ResponseEntity<List<OrderDetail>> getAllOrderDetails() {
      try {
         List<OrderDetail> allOrderDetails = this.orderDetailService.getAllOrderDetails();
         return new ResponseEntity<>(allOrderDetails, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/orders/{id}/order-details"})
   public ResponseEntity<List<OrderDetail>> getAllOrderDetailsOfOrder(@PathVariable("id") Integer id) {
      try {
         List<OrderDetail> listOrderDetails = this.orderDetailService.getAllOrderDetailsOfOrder(id);
         return new ResponseEntity<>(listOrderDetails, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/orders/{id}/sizes/{sizeId}/order-details"})
   public ResponseEntity<OrderDetail> createOrderDetail(@PathVariable("id") Integer id, @RequestBody OrderDetail pOrderDetail, @PathVariable("sizeId") Integer sizeId) {
      try {
         OrderDetail orderDetailCreated = this.orderDetailService.createOrderDetail(id, pOrderDetail, sizeId);
         return new ResponseEntity<>(orderDetailCreated, HttpStatus.OK);
      } catch (Exception var5) {
         System.out.println(var5);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
