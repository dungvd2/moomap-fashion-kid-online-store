// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.repository.CustomerRepository;
import com.devcamp.moomapstorebackend.repository.OrderRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;
import com.devcamp.moomapstorebackend.security.UserPrincipal;
import com.devcamp.moomapstorebackend.service.ExcelExporter;
import com.devcamp.moomapstorebackend.service.OrderService;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class OrderController {
   @Autowired
   OrderService orderService;

   @Autowired
   OrderRepository iOrder;

   @Autowired
   UserRepository iUser;

   @Autowired
   CustomerRepository iCustomer;

   public OrderController() {
   }

   @GetMapping({ "/orders/{orderId}" })
   public ResponseEntity<Order> getOrderById(@PathVariable("orderId") Integer orderId) {
      try {
         Order order = this.orderService.getOrderById(orderId);
         return new ResponseEntity<>(order, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({ "/customers/{customerId}/orders" })
   public ResponseEntity<Page<Order>> getAllOrderOfCustomer(@PathVariable("customerId") Integer customerId,
         @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
      try {
         Page<Order> orderOfCustomerPagination = this.orderService.getOrderOfCustomerByPage(customerId, page, size);
         return new ResponseEntity<>(orderOfCustomerPagination, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // Hàm lấy ra thông tin đơn hàng bằng order code
   // Người nào đặt người đó ms xem được
   @GetMapping("users/orders/detail")
   public ResponseEntity<Object> getOrderByOrderCode(@RequestParam String code) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Order order = iOrder.findByOrderCode(code);
         Customer customerOfOrder = order.getCustomer();
         // Kiểm tra xem 2 id có trùng khớp
         if (customer.getId() == customerOfOrder.getId()) {
            return ResponseEntity.ok(order);
         } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                  .body("Chỉ người đặt hàng hoặc quản trị viên mới xem được đơn hàng!");
         }
      } catch (Exception e) {
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
               .body("Có lỗi xảy ra, vui lòng thử lại sau!");
      }
   }

   // @GetMapping({ "/orders" })
   // public ResponseEntity<Page<Order>> getAllOrders(@RequestParam(defaultValue = "0") int page,
   //       @RequestParam(defaultValue = "10") int size) {
   //    try {
   //       Page<Order> orderPagination = this.orderService.getOrderByPage(page, size);
   //       return ResponseEntity.ok(orderPagination);
   //    } catch (Exception var4) {
   //       System.out.println(var4);
   //       return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
   //    }
   // }

   // API lấy ra danh sách đơn hàng của user đã đăng nhập có phân trang
   @GetMapping("/orders/list")
   public ResponseEntity<Page<Order>> getListOrderOfUser(@RequestParam(defaultValue = "") String orderCode, 
                                                   @RequestParam(defaultValue = "") String status, 
                                                   @RequestParam(defaultValue = "0") int page, 
                                                   @RequestParam(defaultValue = "5") int size) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         List<Order> listOrder = iOrder.findByCustomerIdAndOrderCodeLikeAndStatusLike(customer.getId(), orderCode, status);
         Collections.reverse(listOrder);
         Pageable pageable = PageRequest.of(page, size);
         int start = page * size;
         int end = Math.min(listOrder.size(), start + size);
         Page<Order> orderPage = new PageImpl<>(listOrder.subList(start, end), pageable, listOrder.size());

         return new ResponseEntity<>(orderPage, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({ "/customers/{customerId}/orders" })
   public ResponseEntity<Order> createOrderByCustomerId(@PathVariable("customerId") Integer customerId,
         @RequestBody Order pOrder) {
      try {
         Order orderCreated = this.orderService.createOrderByCustomerId(pOrder, customerId);
         return new ResponseEntity<>(orderCreated, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API dùng để tạo đơn hàng trong trường hợp đăng nhập có bổ sung thông
   // tin khách hàng hoặc từ khách chưa đăng nhập
   @PostMapping({ "/orders/" })
   public ResponseEntity<Order> createNewOrder(@RequestBody Order pOrder) {
      try {
         Order order = this.orderService.createOrder(pOrder);
         Order createdOrder = iOrder.save(order);
         return new ResponseEntity<>(createdOrder, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API để tạo đơn hàng có đăng nhập nhưng chưa bổ sung thông tin khách hàng
   @PostMapping("users/orders")
   public ResponseEntity<Object> orderByLoginWithoutCustomer(@RequestBody Order pOrder) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         Order order = this.orderService.createOrder(pOrder);
         Customer customer = order.getCustomer();
         User user = iUser.findByUsername(userPrincipal.getUsername());
         // Nếu số điện thoại đã được đăng ký bởi người dùng liên kết user khác thì không cho đặt hàng
         if(customer.getUser() != null && customer.getUser() != user) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Số điện thoại đã được sử dụng bởi 1 khách hàng khác vui lòng thay đổi số điện thoại!");
         }
         user.setCustomer(customer);
         customer.setUser(user);
         iUser.save(user);
         iCustomer.save(customer);
         Order createdOrder = iOrder.save(order);
         return new ResponseEntity<>(createdOrder, HttpStatus.OK);
      } catch (Exception e) {
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại sau!");
      }
   }

   // Hàm hủy đơn hàng bởi khách hàng, chỉ có khách hàng nào mới hủy
   // được đơn của khách hàng đó
   @PutMapping("users/cancel-order")
   public ResponseEntity<Object> cancelOrderByUser(@RequestParam String code) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Order order = iOrder.findByOrderCode(code);
         Customer customerOfOrder = order.getCustomer();
         // Kiểm tra xem 2 id có trùng khớp
         if (order.getStatus().equals("processing") && customer.getId() == customerOfOrder.getId()) {
            order.setStatus("cancelled");
            iOrder.save(order);
            return ResponseEntity.ok("Hủy đơn hàng thành công!");
         } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                  .body("Chỉ người đặt hàng hoặc quản trị viên mới hủy được đơn hàng!");
         }
      } catch (Exception e) {
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
               .body("Có lỗi xảy ra, vui lòng thử lại sau!");
      }
   }

   @PutMapping({ "orders/{orderId}" })
   public ResponseEntity<Order> updateOrder(@RequestBody Order pOrder, @PathVariable("orderId") Integer orderId) {
      try {
         Order order = this.orderService.updateOrderStatus(orderId, pOrder);
         return new ResponseEntity<>(order, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({ "orders/{orderId}" })
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Order> deleteOrder(@PathVariable("orderId") Integer orderId) {
      try {
         this.orderService.deledeOrder(orderId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({ "/orders/report/day" })
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<Object[]>> getReportOrderByDay() {
      try {
         return new ResponseEntity<>(iOrder.getReportAmmountByDate(), HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({ "/orders/report/week" })
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<Object[]>> getReportOrderByWeek() {
      try {
         return new ResponseEntity<>(iOrder.getReportAmmountByWeek(), HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({ "/orders/report/month" })
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<Object[]>> getReportOrderByMonth() {
      try {
         return new ResponseEntity<>(iOrder.getReportAmmountByMonth(), HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({ "orders/export" })
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public void exportOrdersToExcel(HttpServletResponse response,
         @RequestParam(defaultValue = "2000-01-01") String start, @RequestParam(defaultValue = "2040-01-01") String end)
         throws IOException {
      response.setContentType("application/octet-stream");
      DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
      String currentDateTime = dateFormatter.format(new Date());
      String headerKey = "Content-Disposition";
      String headerValue = "attachment; filename=orders_" + currentDateTime + ".xlsx";
      response.setHeader(headerKey, headerValue);
      List<Order> listOrderByRange = iOrder.getOrdersByTimeRange(start, end);
      ExcelExporter excelExporter = new ExcelExporter(listOrderByRange);
      excelExporter.export(response);
   }

   // API để lấy thông tin doanh số, và tổng đơn hàng
   @GetMapping("/orders/total")
   public ResponseEntity<Object> getTotalPaymentAndNumberOrders() {
      try {
         return new ResponseEntity<Object>(iOrder.getReportTotalPaymentAndNumberOrders(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API để lọc dữ liệu khách hàng
   @GetMapping("/orders/filter")
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Page<Order>> filterOrderByPage(@RequestParam(defaultValue = "") Integer id,
         @RequestParam(defaultValue = "") String name,
         @RequestParam(defaultValue = "") String code,
         @RequestParam(defaultValue = "c") String status,
         @RequestParam(defaultValue = "time") String type,
         @RequestParam(defaultValue = "0") int page,
         @RequestParam(defaultValue = "10") int size) {
      try {
         Page<Order> pageOrder = orderService.filterOrderByPage(id, name, status, type, code, page, size);
         return new ResponseEntity<>(pageOrder, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

}
