// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Province;
import com.devcamp.moomapstorebackend.repository.ProvinceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService {
   @Autowired
   ProvinceRepository iProvince;

   public ProvinceService() {
   }

   public List<Province> getAll() {
      return this.iProvince.findAll();
   }

   public Province createProvince(Province pProvince) {
      Province provinceCreated = (Province)this.iProvince.save(pProvince);
      return provinceCreated;
   }

   public Province updateProvince(Province pProvince, Integer provinceId) {
      Province vProvince = (Province)this.iProvince.findById(provinceId).get();
      vProvince.setName(pProvince.getName());
      vProvince.setCode(pProvince.getCode());
      Province updatedProvince = (Province)this.iProvince.save(vProvince);
      return updatedProvince;
   }

   public void deleteProvince(Integer provinceId) {
      this.iProvince.deleteById(provinceId);
   }
}
