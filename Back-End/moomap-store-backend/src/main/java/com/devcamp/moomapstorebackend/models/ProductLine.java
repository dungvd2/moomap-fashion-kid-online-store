// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(
   name = "product_lines"
)
public class ProductLine {
   @Id
   @GeneratedValue(
      strategy = GenerationType.IDENTITY
   )
   private int id;
   @Column(
      name = "product_line",
      unique = true
   )
   private String productLine;
   private String description;
   @OneToMany(
      mappedBy = "productLine",
      cascade = {CascadeType.ALL}
   )
   private List<Product> products;

   public ProductLine() {
   }

   public ProductLine(int id, String productLine, String description, List<Product> products) {
      this.id = id;
      this.productLine = productLine;
      this.description = description;
      this.products = products;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getProductLine() {
      return this.productLine;
   }

   public void setProductLine(String productLine) {
      this.productLine = productLine;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public List<Product> getProducts() {
      return this.products;
   }

   public void setProducts(List<Product> products) {
      this.products = products;
   }
}
