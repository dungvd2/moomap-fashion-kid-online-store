// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.models.OrderDetail;
import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.models.Rating;
import com.devcamp.moomapstorebackend.repository.CustomerRepository;
import com.devcamp.moomapstorebackend.repository.OrderRepository;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import com.devcamp.moomapstorebackend.repository.RatingRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RatingService {
   @Autowired
   RatingRepository iRating;
   
   @Autowired
   ProductRepository iProduct;

   @Autowired
   CustomerRepository iCustomer;

   @Autowired
   OrderRepository iOrder;

   @Autowired
   UserRepository iUser;

   public RatingService() {
   }

   public List<Rating> getAllRatingOfProduct(Integer productId) {
      return this.iRating.findAllByProductId(productId);
   }

   public Rating createRating(Rating pRating, Integer productId) {
      Product product = (Product)this.iProduct.findById(productId).get();
      pRating.setProduct(product);
      Rating ratingCreated = (Rating)this.iRating.save(pRating);
      return ratingCreated;
   }
   
   // Hàm trả về danh sách các đánh giá có thể hiển thị trên trang customer
   public List<Rating> getRatingCanDisplay(String productName) {
      Product product = iProduct.findByProductName(productName);
      List<Rating> allRatings = product.getRatings();
      List<Rating> canDisplayRating = new ArrayList<>();
      for(Rating rating: allRatings) {
         if(rating.getStatus().equals("approved")) {
            canDisplayRating.add(rating);
         }
         else if(rating.getStatus().equals("pending") && rating.getUpdateRatingDetail() != null) {
            rating.setRatingDetail(rating.getUpdateRatingDetail());
            rating.setRatingPoint(rating.getUpdateRatingPoint());
            canDisplayRating.add(rating);  
         }
      }
      return canDisplayRating;
   }

   public void deleteRating(Integer ratingId) {
      this.iRating.deleteById(ratingId);
   }

   // Hàm để kiểm tra xem người dùng đã mua sản phẩm chưa
   public Boolean isUserBoughtProduct(Customer customer, String productName) {
      List<Order> listorder = customer.getOrders();
      if(listorder == null) {
         return false;
      }
      for(Order order: listorder) {
         if(order.getStatus().equals("confirmed")) {
            List<OrderDetail> listOrderDetails = order.getOrderDetails();
            for(OrderDetail detail : listOrderDetails) {
               if(productName.equals(detail.getProductName())) {
                  return true;
               }
            }
         }
      }
      return false;
   }

   // Hàm kiểm tra xem người dùng đã đánh giá sản phẩm chưa
   public Boolean isUserRatedProduct(Customer customer, String productName) {
      Product product = iProduct.findByProductName(productName);
      Rating rating = iRating.findByProductIdAndCustomerId(product.getId(), customer.getId());
      if(rating != null) {
         return true;
      }
      return false;
   }

   // Hàm post đánh giá cho user
   public Rating createRatingForProduct(Rating rating, String productName, Customer customer) {
      Product product = iProduct.findByProductName(productName);
      rating.setStatus("pending");
      rating.setProduct(product);
      rating.setCustomer(customer);
      Rating ratingCreated = iRating.save(rating);
      return ratingCreated;
   }

   // Hàm sửa đánh giá cho người dùng
   public Rating updateRating(Rating pRating, String productName, Customer customer) {
      Rating rating = this.findRating(productName, customer);
      rating.setUpdateRatingDetail(rating.getRatingDetail());
      rating.setUpdateRatingPoint(rating.getRatingPoint());
      rating.setRatingDetail(pRating.getRatingDetail());
      rating.setStatus("pending");
      rating.setRatingPoint(pRating.getRatingPoint());
      Rating ratingUpdated = iRating.save(rating);
      return ratingUpdated;
   }

   // Hàm cập nhận hiển thị đánh giá cho admin
   public Rating updateRatingForAdmin(Integer ratingId, String status) {
      Rating rating = iRating.findById(ratingId).get();
      rating.setStatus(status);
      rating.setUpdateRatingDetail(null);
      rating.setUpdateRatingPoint(null);
      Rating ratingUpdated = iRating.save(rating);
      return ratingUpdated;
   }

   // Hàm tìm kiếm rating bằng productName và customer Id
   public Rating findRating(String productName, Customer customer) {
      Product product = iProduct.findByProductName(productName);
      Rating rating = iRating.findByProductIdAndCustomerId(product.getId(), customer.getId());
      return rating;
   }


   // Hàm lọc khách hàng có phân trang
   public Page<Rating> filterRatingByPage(String productName, String status, Float point, int page, int size) {
      List<Rating> ratings = iRating.filterRatings(productName, point, status);
      Collections.reverse(ratings);
      Pageable pageable = PageRequest.of(page, size);
      
      int start = page * size;
      int end = Math.min(start + size, ratings.size());
      Page<Rating> ratingPageNaviPage = new PageImpl<>(ratings.subList(start, end), pageable, ratings.size());
      return ratingPageNaviPage;
   }
}
