// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.ProductLine;
import com.devcamp.moomapstorebackend.repository.ProductLineRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductLineService {
   @Autowired
   ProductLineRepository iProductLine;

   public ProductLineService() {
   }

   public List<ProductLine> getAllProductLine() {
      return this.iProductLine.findAll();
   }

   public ProductLine createProductLine(ProductLine pProductLine) {
      return (ProductLine)this.iProductLine.save(pProductLine);
   }

   public ProductLine getProductLineById(Integer productLineId) {
      return (ProductLine)this.iProductLine.findById(productLineId).get();
   }

   public ProductLine updateProductLine(ProductLine pProductLine, Integer productLineId) {
      ProductLine productLine = (ProductLine)this.iProductLine.findById(productLineId).get();
      productLine.setDescription(pProductLine.getDescription());
      productLine.setProductLine(pProductLine.getProductLine());
      ProductLine productLineUpdated = (ProductLine)this.iProductLine.save(productLine);
      return productLineUpdated;
   }

   public void deleteProductLine(Integer productLineId) {
      this.iProductLine.deleteById(productLineId);
   }
}
