package com.devcamp.moomapstorebackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.moomapstorebackend.entity.Role;


public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleKey(String roleKey);
}
