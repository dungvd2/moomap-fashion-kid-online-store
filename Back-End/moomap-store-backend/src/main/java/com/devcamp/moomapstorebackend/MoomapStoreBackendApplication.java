package com.devcamp.moomapstorebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MoomapStoreBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoomapStoreBackendApplication.class, args);
	}

}
