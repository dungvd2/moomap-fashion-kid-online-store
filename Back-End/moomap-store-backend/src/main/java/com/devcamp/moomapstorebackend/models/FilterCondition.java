package com.devcamp.moomapstorebackend.models;

import java.util.List;

public class FilterCondition {
    private List<String> genders;
    
    private List<String> lines;
    
    private List<Integer> prices;
    
    private String type;

    public FilterCondition() {
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public List<Integer> getPrices() {
        return prices;
    }

    public void setPrices(List<Integer> prices) {
        this.prices = prices;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
