package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
