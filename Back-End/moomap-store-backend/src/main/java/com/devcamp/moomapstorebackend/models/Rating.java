// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(
   name = "rating"
)
public class Rating {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   
   @Column(name = "rating_point", nullable = false)
   private @NotNull Float ratingPoint;
   
   @Column(name = "rating_detail", length = 1000, nullable = false)
   private @NotEmpty String ratingDetail;
   
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "product_id")
   @JsonIgnore
   private Product product;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "customer_id")
   @JsonIgnore
   private Customer customer;

   private Float updateRatingPoint;

   private String updateRatingDetail;
   
   private String status;

   @Transient
   private String productName;

   @Transient
   private String customerName;

   public Rating() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getRatingDetail() {
      return this.ratingDetail;
   }

   public void setRatingDetail(String ratingDetail) {
      this.ratingDetail = ratingDetail;
   }

   public Product getProduct() {
      return this.product;
   }

   public void setProduct(Product product) {
      this.product = product;
   }

   public Float getRatingPoint() {
      return ratingPoint;
   }

   public void setRatingPoint(Float ratingPoint) {
      this.ratingPoint = ratingPoint;
   }

   public Customer getCustomer() {
      return customer;
   }

   public void setCustomer(Customer customer) {
      this.customer = customer;
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getUpdateRatingDetail() {
      return updateRatingDetail;
   }

   public void setUpdateRatingDetail(String updateRatingDetail) {
      this.updateRatingDetail = updateRatingDetail;
   }

   public Float getUpdateRatingPoint() {
      return updateRatingPoint;
   }

   public void setUpdateRatingPoint(Float updateRatingPoint) {
      this.updateRatingPoint = updateRatingPoint;
   }

   public String getProductName() {
      return this.product.getProductName();
   }

   public String getCustomerName() {
      return this.customer.getFirstName() + " " + this.customer.getLastName();
   }

   
}
