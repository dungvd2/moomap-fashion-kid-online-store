// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Rating;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RatingRepository extends JpaRepository<Rating, Integer> {
   List<Rating> findAllByProductId(Integer productId);

   Rating findByProductIdAndCustomerId(Integer productId, Integer customerId);

   // Hàm lấy ra danh sách các Đánh giá đáp ứng các điều kiện sao, trạng thái
   @Query(value = "SELECT r.* FROM rating r INNER JOIN products p ON r.product_id = p.id WHERE p.product_name LIKE %:productName% AND (:point IS NULL OR r.rating_point =:point) AND r.status LIKE %:status% ORDER BY r.status ASC", nativeQuery = true)
   List<Rating> filterRatings(@Param("productName") String productName, @Param("point") Float point, @Param("status") String status);
}
