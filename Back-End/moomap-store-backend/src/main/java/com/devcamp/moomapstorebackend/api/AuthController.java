package com.devcamp.moomapstorebackend.api;

import java.util.Collection;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.moomapstorebackend.entity.Role;
import com.devcamp.moomapstorebackend.entity.Token;
import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.NewPasswordData;
import com.devcamp.moomapstorebackend.repository.RoleRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;
import com.devcamp.moomapstorebackend.security.JwtUtil;
import com.devcamp.moomapstorebackend.security.UserPrincipal;
import com.devcamp.moomapstorebackend.service.TokenService;
import com.devcamp.moomapstorebackend.service.UserService;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired 
    private UserRepository iUser;

    @Autowired
    private JwtUtil jwtUtil;
    
    @Autowired 
    private RoleRepository iRole;

    @CrossOrigin
    @PostMapping("/register")
    public User register(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Role customerRole = iRole.findByRoleKey("ROLE_CUSTOMER");
        user.getRoles().add(customerRole);
        return userService.createUser(user);
    }
    
    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        User pUser = iUser.findByUsername(user.getUsername());
        // Biến mật khẩu vừa nhập thành chuỗi mã hóa rồi ms so sánh vs chuỗi mã hóa đã có ở mật khẩu đã tạo
        if (null == userPrincipal || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tên đăng nhập hoặc mật khẩu không đúng!");
        }
        // Kiểm tra xem có trong tình trạng xóa hay không
        if(pUser.isDeleted()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tài khoản hiện không hoạt động!");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        return ResponseEntity.ok(token.getToken());
        
    }

    // API thay đổi mật khẩu
    @CrossOrigin
    @PostMapping("/users/change-password")
    public ResponseEntity<String> changePassword(@RequestBody NewPasswordData passData) {
        User user = iUser.findByUsername(passData.getUsername());
        if(user == null || !new BCryptPasswordEncoder().matches(passData.getOldPass(), user.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mật khẩu hiện tại chưa chính xác!");
        }
        String passwordRegex = "^(?=.*[a-zA-Z])(?=.*\\d).{6,}$";
        Pattern pattern = Pattern.compile(passwordRegex);
        Matcher matcher = pattern.matcher(passData.getNewPass());
        if(!matcher.matches()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mật khẩu phải có ít nhất 6 ký tự bao gồm cả chữ lẫn số!");
        }
        user.setPassword(new BCryptPasswordEncoder().encode(passData.getNewPass()));
        iUser.save(user);
        return ResponseEntity.ok("Mật khẩu đã được thay đổi!");
    }


    @GetMapping("/hello")
    @CrossOrigin
	/*
	 * @PreAuthorize("hasRole('USER_READ') " + "|| hasRole('USER_CREATE')" +
	 * "|| hasRole('USER_UPDATE')" + "|| (hasRole('USER_DELETE')")
	 */
    public ResponseEntity hello() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
        Collection<GrantedAuthority> authorities = userPrincipal.getAuthorities();
        Set<String> roles = authentication.getAuthorities().stream()
        	     .map(r -> r.getAuthority()).collect(Collectors.toSet());
        boolean hasUserRole = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_USER"));
        return ResponseEntity.ok("Hi " + userPrincipal.getUsername());
    }
    
    @GetMapping("/hello1")
    @CrossOrigin
	/*
	 * @PreAuthorize("hasRole('USER_READ') " + "|| hasRole('USER_CREATE')" +
	 * "|| hasRole('USER_UPDATE')" + "|| (hasRole('USER_DELETE')")
	 */
    public ResponseEntity hello1() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
        return ResponseEntity.ok(userPrincipal);
    }

    @GetMapping("/users/check")
    @CrossOrigin
    public ResponseEntity checkCustomer() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
            User user = iUser.findByUsername(userPrincipal.getUsername());
            Customer customer = user.getCustomer();
            return new ResponseEntity<>(customer, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

    @GetMapping("/hello2")
    @PreAuthorize("hasAnyAuthority('USER_READ','USER_DELETE')")
//    @PreAuthorize("hasRole('USER_READ') " +
//            "|| (hasRole('USER_DELETE')")
    public ResponseEntity hello2() {
        return ResponseEntity.ok("hello 2 have USER_READ OR USER_DELETE");
    }
    
    @GetMapping("/hello3")
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ResponseEntity hello3() {
        return ResponseEntity.ok("hello cho ADMIN va CUSTOMER");
    }
    
    @GetMapping("/hello4")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity hello4() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }
    @GetMapping("/hello6")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity hello6() {
        return ResponseEntity.ok("hello chi cho mANAGER");
    }
    @GetMapping("/hello7")
    @PreAuthorize("hasAnyAuthority('USER_DELETE')")
//    @PreAuthorize("hasRole('USER_READ') " +
//            "|| (hasRole('USER_DELETE')")
    public ResponseEntity hello7() {
        return ResponseEntity.ok("hello 2 have USER_DELETE");
    }
    @GetMapping("/hello5")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity hello5() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }
    
}
