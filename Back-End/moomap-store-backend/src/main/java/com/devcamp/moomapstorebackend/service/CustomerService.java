// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
   @Autowired
   CustomerRepository iCustomer;

   public CustomerService() {
   }

   public List<Customer> getAllCustomer() {
      return this.iCustomer.findAll();
   }

   public Customer getCustomerById(Integer customerId) {
      return (Customer)this.iCustomer.findById(customerId).get();
   }

   public Customer createCustomer(Customer pCustomer) {
      pCustomer.setRank("vip");
      pCustomer.setNumOrder(0);
      Customer customerCreated = (Customer)this.iCustomer.save(pCustomer);
      return customerCreated;
   }

   public Customer getCustomerByPhoneNumber(String phoneNumber) {
      Customer customer = this.iCustomer.findByPhoneNumber(phoneNumber);
      return customer;
   }

   // Tạo mới thông tin khách hàng thông qua thông tin order
   public Customer createCustomerByOrder(Order pOrder) {
      Customer customer = new Customer();
      customer.setAddress(pOrder.getAddress());
      customer.setDistrict(pOrder.getDistrict());
      customer.setEmail(pOrder.getEmail());
      customer.setWard(pOrder.getWard());
      customer.setProvince(pOrder.getProvince());
      customer.setFirstName(pOrder.getFirstName());
      customer.setLastName(pOrder.getLastName());
      customer.setPhoneNumber(pOrder.getPhoneNumber());
      customer.setNumOrder(1);
      // Cập nhật xếp hạng khách hàng thông qua tổng thanh toán
      Long vTotalPayment = customer.getAmountPayment();
      String vCustomerRank = "";
      if(vTotalPayment >= 5000000) {
         vCustomerRank = "plantinum";
      } 
      else if(vTotalPayment >= 2000000) {
         vCustomerRank = "gold";
      }
      else if(vTotalPayment >= 1000000) {
         vCustomerRank = "silver";
      }
      else {
         vCustomerRank = "vip";
      }
      customer.setRank(vCustomerRank);
      Customer customerCreated = (Customer)this.iCustomer.save(customer);
      return customerCreated;
   }

   // Cập nhật thông tin khách hàng
   public Customer updateCustomer(Customer pCustomer, Integer customerId) {
      String updatedPhoneNumber = pCustomer.getPhoneNumber();
      Customer existingCustomerWithPhoneNumber = iCustomer.findByPhoneNumber(updatedPhoneNumber);
      if(existingCustomerWithPhoneNumber != null && existingCustomerWithPhoneNumber.getId() != customerId) {
         throw new IllegalArgumentException("Số điện thoại đã được sử dụng bởi khách hàng khác!");
      }
      Customer vCustomer = iCustomer.findById(customerId).orElse(null);
      if(vCustomer != null) {
         vCustomer.setAddress(pCustomer.getAddress());
         vCustomer.setDistrict(pCustomer.getDistrict());
         vCustomer.setEmail(pCustomer.getEmail());
         vCustomer.setFirstName(pCustomer.getFirstName());
         vCustomer.setWard(pCustomer.getWard());
         vCustomer.setProvince(pCustomer.getProvince());
         vCustomer.setLastName(pCustomer.getLastName());
         vCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
         Customer customerUpdated = (Customer)this.iCustomer.save(vCustomer);
         return customerUpdated;
      }
      else {
         throw new IllegalArgumentException("Không tìm thấy khách hàng với ID đã cho!");
      }
   }

   // Cập nhật thông tin khách hàng thông qua thông tin order
   public Customer updateCustomerByOrder(Customer customer, Order pOrder) {
      customer.setAddress(pOrder.getAddress());
      customer.setDistrict(pOrder.getDistrict());
      customer.setEmail(pOrder.getEmail());
      customer.setWard(pOrder.getWard());
      customer.setProvince(pOrder.getProvince());
      customer.setFirstName(pOrder.getFirstName());
      customer.setLastName(pOrder.getLastName());
      customer.setNumOrder(customer.getNumOrder() + 1);
      Long vTotalPayment = customer.getAmountPayment();
      String vCustomerRank = "";
      if(vTotalPayment >= 5000000) {
         vCustomerRank = "plantinum";
      } 
      else if(vTotalPayment >= 2000000) {
         vCustomerRank = "gold";
      }
      else if(vTotalPayment >= 1000000) {
         vCustomerRank = "silver";
      }
      else {
         vCustomerRank = "vip";
      }
      customer.setRank(vCustomerRank);
      Customer customerCreated = (Customer)this.iCustomer.save(customer);
      return customerCreated;
   }

   public void deleteCustomer(Integer customerId) {
      this.iCustomer.deleteById(customerId);
   }

   public List<Customer> getCustomers(int page, int size) {
      Pageable pageable = PageRequest.of(page - 1, size);
      Page<Customer> customerPage = this.iCustomer.findAll(pageable);
      return customerPage.getContent();
   }

   // Hàm lấy ra taofn bộ khách hàng có phân trang
   public Page<Customer> getCustomerByPage(int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Customer> customers = new ArrayList<>();
      iCustomer.findAll(pageable).forEach(customers::add);
      Integer totalElement = (int)iCustomer.count();
      Page<Customer> customerPageNavigate = new PageImpl<>(customers, pageable, totalElement);
      return customerPageNavigate;
   }

   // Hàm lọc dữ liệu khách hàng có phân trang
   public Page<Customer> getCustomerFilterByPage(String name, String type, String rank, String phone, int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Customer> customers = iCustomer.filterCustomer(name, type, rank, phone);
      int start = page * size;
      int end = Math.min(start + size, customers.size());
      Page<Customer> customerPageNavigate = new PageImpl<>(customers.subList(start, end), pageable, customers.size());
      return customerPageNavigate;
   }
}
