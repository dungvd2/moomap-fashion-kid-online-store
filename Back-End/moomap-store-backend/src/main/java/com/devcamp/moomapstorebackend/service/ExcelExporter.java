// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Order;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExporter {
   private XSSFWorkbook workbook;
	private XSSFSheet sheet;
   private List<Order> orders;



   public ExcelExporter(List<Order> orders) {
      this.orders = orders;
      this.workbook = new XSSFWorkbook();
   }

   private void createCell(Row row, int columnCount, Object value, CellStyle style) {
      this.sheet.autoSizeColumn(columnCount);
      Cell cell = row.createCell(columnCount);
      if (value instanceof Integer) {
         cell.setCellValue((Integer)value);
      } else if (value instanceof Boolean) {
         cell.setCellValue((Boolean)value);
      } else {
         cell.setCellValue((String)value);
      }

      cell.setCellStyle(style);
   }

   private void writeHeaderLine() {
      this.sheet = this.workbook.createSheet("Orders");
      Row row = this.sheet.createRow(0);
      CellStyle style = this.workbook.createCellStyle();
      XSSFFont font = this.workbook.createFont();
      font.setBold(true);
      font.setFontHeight(16.0);
      style.setFont(font);
      this.createCell(row, 0, "Mã đơn hàng", style);
      this.createCell(row, 1, "Tên khách hàng", style);
      this.createCell(row, 2, "Địa chỉ nhận hàng", style);
      this.createCell(row, 3, "Số điện thoại", style);
      this.createCell(row, 4, "Email", style);
      this.createCell(row, 5, "Lời nhắn", style);
      this.createCell(row, 6, "Ngày đặt hàng", style);
      this.createCell(row, 7, "Tổng thanh toán", style);
      this.createCell(row, 8, "Trạng thái", style);
   }

   private void writeDataLine() {
      int rowCount = 1;
      CellStyle style = this.workbook.createCellStyle();
      XSSFFont font = this.workbook.createFont();
      font.setFontHeight(14.0);
      style.setFont(font);
      for(Order order: this.orders) {
         Row row = sheet.createRow(rowCount++);
         int columnCount = 0;
         this.createCell(row, columnCount++, order.getOrderCode(), style);
         this.createCell(row, columnCount++, order.getFirstName() + " " + order.getLastName(), style);
         this.createCell(row, columnCount++, order.getUserAddress(), style);
         this.createCell(row, columnCount++, order.getPhoneNumber(), style);
         this.createCell(row, columnCount++, order.getEmail(), style);
         this.createCell(row, columnCount++, order.getMessage(), style);
         this.createCell(row, columnCount++, order.getOrderDate().toString(), style);
         this.createCell(row, columnCount++, order.getAmmount(), style);
         this.createCell(row, columnCount++, order.getStatus(), style);
      }
   }

   public void export(HttpServletResponse response) throws IOException {
      writeHeaderLine();
      writeDataLine();

      ServletOutputStream outputStream = response.getOutputStream();
      workbook.write(outputStream);
      workbook.close();

      outputStream.close();
   }

}
