// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "orders")
public class Order {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "order_code", unique = true)
   private String orderCode;
   
   @Column(name = "first_name")
   private @NotNull(message = "Input first name") String firstName;
   
   @Column(name = "last_name")
   private @NotNull(message = "Input last name") String lastName;
   
   @Column(name = "phone_number")
   private @NotNull(message = "Input phonenumber") String phoneNumber;
   
   private String email;
   
   private String province;
   
   private String district;
   
   private String ward;
   
   @Column(name = "address")
   private @NotNull(message = "Input address detail") String address;
   
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "order_date")
   private Date orderDate;
   
   private String status;
   
   private String message;
   
   @NotNull(message = "input total amount")
   private Integer ammount;
   
   @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
   private List<OrderDetail> orderDetails;
   
   @Transient
   private String userAddress;
   
   @ManyToOne
   @JoinColumn(name = "customer_id")
   @JsonIgnore
   private Customer customer;

   public Order() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public Date getOrderDate() {
      return this.orderDate;
   }

   public void setOrderDate(Date orderDate) {
      this.orderDate = orderDate;
   }

   public String getStatus() {
      return this.status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public Customer getCustomer() {
      return this.customer;
   }

   public void setCustomer(Customer customer) {
      this.customer = customer;
   }

   public Integer getAmmount() {
      return this.ammount;
   }

   public void setAmmount(Integer ammount) {
      this.ammount = ammount;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public void setProvince(String province) {
      this.province = province;
   }

   public void setDistrict(String district) {
      this.district = district;
   }

   public void setWard(String ward) {
      this.ward = ward;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getLastName() {
      return this.lastName;
   }

   public String getFirstName() {
      return this.firstName;
   }

   public String getPhoneNumber() {
      return this.phoneNumber;
   }

   public String getAddress() {
      return this.address;
   }

   public String getProvince() {
      return this.province;
   }

   public String getDistrict() {
      return this.district;
   }

   public String getWard() {
      return this.ward;
   }

   public String getEmail() {
      return this.email;
   }

   public String getUserAddress() {
      return this.address + ", " + this.ward + ", " + this.district + ", " + this.province;
   }

   public String getOrderCode() {
      return this.orderCode;
   }

   public void setOrderCode(String orderCode) {
      this.orderCode = orderCode;
   }

   public List<OrderDetail> getOrderDetails() {
      return this.orderDetails;
   }

   public void setOrderDetails(List<OrderDetail> orderDetails) {
      this.orderDetails = orderDetails;
   }

   public void setUserAddress(String userAddress) {
      this.userAddress = userAddress;
   }

}
