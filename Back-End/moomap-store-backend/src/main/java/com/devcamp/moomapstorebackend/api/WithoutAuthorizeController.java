package com.devcamp.moomapstorebackend.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.moomapstorebackend.entity.Role;
import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.repository.RoleRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;
import com.devcamp.moomapstorebackend.service.UserService;

@RestController
public class WithoutAuthorizeController {
	@Autowired
	private UserRepository userRepository;

     @Autowired 
    private RoleRepository iRole;
    /**
     * Test trường hợp admin lấy là danh sách user
     * @return
     */

    private String guaranteeCode = "120798";

    @CrossOrigin
    @GetMapping("/users/filter")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Page<User>> filterUser(@RequestParam(defaultValue = "") String username,
                                                 @RequestParam(defaultValue = "all") String isdeleted,
                                                 @RequestParam(defaultValue = "") String roleKey,
                                                 @RequestParam(defaultValue = "0") int page,
                                                 @RequestParam(defaultValue = "10") int size) {
        try {

            List<User> listUser = new ArrayList<>();
            
            if(isdeleted.equals("all")) {
                listUser = userRepository.filterUserWithoutDeletedAccount(username, roleKey);
            }
            else if(isdeleted.equals("true")) {
                listUser = userRepository.filterUser(username, true ,roleKey);
            }
            else if(isdeleted.equals("false")) {
                listUser = userRepository.filterUser(username, false ,roleKey);
            }

            Pageable pageable = PageRequest.of(page, size);

            int start = page * size;
            int end = Math.min(start + size, listUser.size());
            Page<User>  userPage = new PageImpl<>(listUser.subList(start, end), pageable, listUser.size());
            return new ResponseEntity<> (userPage,HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Test trường hợp khôngcheck quyền Authorize
     * Tạo mới user
     * @param user
     * @return
     */
    @PostMapping("/users/createAdmin")
    public ResponseEntity<Object> create(@RequestBody User user) {
    	user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
    	return new ResponseEntity<>(userRepository.save(user),HttpStatus.CREATED);
    }
    @Autowired
    private UserService userService;
    /**
     * Test có kiểm tra quyền.
     * @param user
     * @return
     */
    @CrossOrigin
    @PostMapping("/users/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> adminCreate(@RequestBody User user, @RequestParam String roleKey, @RequestParam(defaultValue = "") String passcode) {
        try {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            if(roleKey.equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ chưa chính xác!");
            } 
            Role customerRole = iRole.findByRoleKey(roleKey);
            user.getRoles().add(customerRole);
            userService.createUser(user);
            return ResponseEntity.ok("Tạo tài khoản thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");

        }
        
    }

    //API Lấy lại mật khẩu bằng câu hỏi bảo mật
    @CrossOrigin
    @PutMapping("users/forgot-password")
    public ResponseEntity<Object> createNewPasswordBySecretQuestionAndAnswer(@RequestBody User pUser) {
        try {
            User user = userRepository.findByUsername(pUser.getUsername());
            if(user == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tên đăng nhập chưa chính xác!");
            }
            if(!user.getSecretQuestion().equals(pUser.getSecretQuestion()) || !user.getSecretAnswer().equals(pUser.getSecretAnswer())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Câu hỏi hoặc câu trả lời bảo mật chưa chính xác!");
            }
            user.setPassword(new BCryptPasswordEncoder().encode(pUser.getPassword()));
            userRepository.save(user);
            return ResponseEntity.ok("Lấy lại mật khẩu thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");

        }
    } 


    // API cập nhật user
    @CrossOrigin
    @PutMapping("users/update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateUser(@RequestBody User pUser, @RequestParam String rolekey, @RequestParam(defaultValue = "") String passcode) {
        try {
            User user = userRepository.findByUsername(pUser.getUsername());
            if(user == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username không tồn tại!");
            }
            // Thay đổi role
            List<Role> roleList = new ArrayList<>(user.getRoles());
            if(roleList.get(0).getRoleKey().equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không chính xác!");
            }

            // Nếu role hiện tại k phải là AMIN mà muốn chuyển từ role khác qua amin thì sai mã cũng không chấp nhận
            else if(rolekey.equals("ROLE_ADMIN") && !roleList.get(0).getRoleKey().equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không chính xác!");
            }

            

            Role role = iRole.findByRoleKey(rolekey);
            Set<Role> roles = new HashSet<>();
            roles.add(role);
            user.setRoles(roles);
            // Thay đổi password mới
            user.setPassword(new BCryptPasswordEncoder().encode(pUser.getPassword()));

            userRepository.save(user);

            return ResponseEntity.ok("Sửa thông tin thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }


    // API thay đổi role khách hàng
    @CrossOrigin
    @PutMapping("users/update/roles")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> changeRoleUser(@RequestParam String username, @RequestParam String roleKey, @RequestParam(defaultValue = "") String passcode) {
        try {
            User user = userRepository.findByUsername(username);
            
            List<Role> roleList = new ArrayList<>(user.getRoles());
            // Nếu role hiện tại là AMIN và đổi sang các role khác mà mã bảo vệ sai thì không chấp nhận
            if(!roleKey.equals("ROLE_ADMIN") && roleList.get(0).getRoleKey().equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không chính xác!");
            }
            // Nếu role hiện tại k phải là AMIN mà muốn chuyển từ role khác qua amin thì sai mã cũng không chấp nhận
            if(roleKey.equals("ROLE_ADMIN") && !roleList.get(0).getRoleKey().equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không chính xác!");
            }
            
            Role role = iRole.findByRoleKey(roleKey);
            Set<Role> roles = new HashSet<>();
            roles.add(role);
            user.setRoles(roles);
            userRepository.save(user);
            return ResponseEntity.ok("Cập nhật tài khoản thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    // API xóa user
    @CrossOrigin
    @PutMapping("users/remove")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> removeUser(@RequestParam String username, @RequestParam(defaultValue = "") String passcode) {
        try {
            User user = userRepository.findByUsername(username);
            // ADMin thì không được xóa thông tin của Admin
            List<Role> roleList = new ArrayList<>(user.getRoles());
            if(roleList.get(0).getRoleKey().equals("ROLE_ADMIN") && !passcode.equals(guaranteeCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ chưa chính xác!");
            }
            user.setDeleted(true);
            userRepository.save(user);
            return ResponseEntity.ok("Xóa tài khoản thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    // API khôi phục user
    @CrossOrigin
    @PutMapping("users/restore")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> restoreUser(@RequestParam String username) {
        try {
            User user = userRepository.findByUsername(username);
            user.setDeleted(false);
            userRepository.save(user);
            return ResponseEntity.ok("Khôi phục tài khoản thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }
}
