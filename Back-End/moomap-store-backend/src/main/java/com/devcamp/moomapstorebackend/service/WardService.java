// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.District;
import com.devcamp.moomapstorebackend.models.Ward;
import com.devcamp.moomapstorebackend.repository.DistrictRepository;
import com.devcamp.moomapstorebackend.repository.WardRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class WardService {
   @Autowired
   WardRepository iWard;
   @Autowired
   DistrictRepository iDistrict;

   public WardService() {
   }

   public List<Ward> getAll() {
      return this.iWard.findAll();
   }

   public Ward createWard(Ward pWard, Integer districtId) {
      District vDistrict = (District)this.iDistrict.findById(districtId).get();
      pWard.setDistrict(vDistrict);
      Ward wardCreated = (Ward)this.iWard.save(pWard);
      return wardCreated;
   }

   public Ward updateWard(Ward pWard, Integer wardId, Integer districtId) {
      Ward vWard = (Ward)this.iWard.findById(wardId).get();
      District vDistrict = (District)this.iDistrict.findById(districtId).get();
      vWard.setDistrict(vDistrict);
      vWard.setName(pWard.getName());
      vWard.setPrefix(pWard.getPrefix());
      Ward updatedWard = (Ward)this.iWard.save(vWard);
      return updatedWard;
   }

   public void deleteWard(Integer wardId) {
      this.iWard.deleteById(wardId);
   }

   public Page<Ward> getWardByPage(int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Ward> list = new ArrayList<>();
      this.iWard.findAll(pageable).forEach(list::add);
      Integer listSize = (int)this.iWard.count();
      Page<Ward> wardPageNavigation = new PageImpl<>(list, pageable, (long)listSize);
      return wardPageNavigation;
   }
}
