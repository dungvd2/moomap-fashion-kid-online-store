package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
