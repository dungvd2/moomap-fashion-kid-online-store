// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Order;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderRepository extends JpaRepository<Order, Integer> {
   List<Order> findAllByCustomerId(Integer customerId);

   Order findByOrderCode(String orderCode);

   // Tìm kiếm theo customer id và order code Like status like
   @Query(value = "SELECT * FROM orders WHERE customer_id =:customerId AND order_code LIKE %:orderCode% AND status LIKE %:status%", nativeQuery = true)
   List<Order> findByCustomerIdAndOrderCodeLikeAndStatusLike(@Param("customerId") Integer customerId, 
                                                             @Param("orderCode") String orderCode,      
                                                             @Param("status") String status);

   // Báo cáo doanh thu theo ngày
   @Query(
      value = "SELECT date_table.order_date, COALESCE(SUM(orders.ammount), 0) AS total_amount FROM ( SELECT CURDATE() - INTERVAL n.num DAY AS order_date FROM (SELECT 0 AS num UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) AS n ) AS date_table LEFT JOIN orders ON DATE(orders.order_date) = date_table.order_date WHERE date_table.order_date >= CURDATE() - INTERVAL 5 DAY GROUP BY date_table.order_date ORDER BY date_table.order_date ASC;",
      nativeQuery = true
   )
   List<Object[]> getReportAmmountByDate();

   // Báo cáo doanh thu theo tuần
   @Query(  
      value = "SELECT weeks.week AS week, COUNT(orders.order_date) AS order_count, COALESCE(SUM(orders.ammount), 0) AS total_amount FROM ( SELECT DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL n.num WEEK), '%x-W%v') AS week FROM (SELECT 0 AS num UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) AS n ) AS weeks LEFT JOIN orders ON weeks.week = DATE_FORMAT(orders.order_date, '%x-W%v') WHERE weeks.week >= DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 5 WEEK), '%x-W%v') GROUP BY weeks.week ORDER BY weeks.week;",
      nativeQuery = true
   )
   List<Object[]> getReportAmmountByWeek();

   // Báo cáo doanh thu theo tháng
   @Query(
      value = "SELECT date_table.month AS month, COALESCE(SUM(orders.ammount), 0) AS revenue FROM ( SELECT DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL n.num MONTH), '%Y-%m') AS month FROM (SELECT 0 AS num UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) AS n ) AS date_table LEFT JOIN orders ON DATE_FORMAT(orders.order_date, '%Y-%m') = date_table.month WHERE date_table.month >= DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 4 MONTH), '%Y-%m') GROUP BY date_table.month ORDER BY date_table.month;",
      nativeQuery = true
   )
   List<Object[]> getReportAmmountByMonth();

   // Lấy ra danh sách đơn hàng theo từng khoảng thời gian
   @Query(
      value = "SELECT * FROM orders WHERE order_date BETWEEN :start AND :end ORDER BY order_date;",
      nativeQuery = true
   )
   List<Order> getOrdersByTimeRange(@Param("start") String start, @Param("end") String end);

   // Báo cáo tổng doanh số và số đơn hàng
   @Query(value = "SELECT COUNT(*) AS numberOrder, SUM(ammount) AS totalPayment FROM `orders`;", nativeQuery = true)
   Object getReportTotalPaymentAndNumberOrders();

   // Tìm ra số đơn hàng bằng id khách hàng
   @Query(value = "SELECT COUNT(order_code) FROM `orders` WHERE customer_id = :customerId", nativeQuery = true)
   Integer countByCustomerId(@Param("customerId") Integer customerId);

   // Lọc đơn hàng theo tên, theo doanh số...
   @Query(value = "SELECT * FROM orders WHERE CONCAT(first_name, ' ', last_name) LIKE %:name% " +
                  "AND customer_id = COALESCE(:customerId, customer_id) " +
                  "AND order_code LIKE %:code% " +
                  "AND status LIKE %:status% ORDER BY " + 
                  "CASE WHEN :type = 'ASC' THEN ammount END ASC, " + 
                  "CASE WHEN :type = 'DESC' THEN ammount END DESC, " + 
                  "CASE WHEN :type = 'time' THEN order_date END DESC",  nativeQuery = true) 
   List<Order> filterOrders(@Param("customerId") Integer customerId, @Param("name") String name,@Param("status") String status, @Param("type") String type, @Param("code") String code);
}
