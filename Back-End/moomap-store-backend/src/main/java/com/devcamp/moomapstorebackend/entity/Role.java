package com.devcamp.moomapstorebackend.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_role")
public class Role extends BaseEntity {

	private String roleName;

	private String roleKey;

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the roleKey
	 */
	public String getRoleKey() {
		return roleKey;
	}

	/**
	 * @param roleKey the roleKey to set
	 */
	public void setRoleKey(String roleKey) {
		this.roleKey = roleKey;
	}


}
