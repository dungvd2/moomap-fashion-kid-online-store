// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.District;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DistrictRepository extends JpaRepository<District, Integer> {
   @Query(
      value = "SELECT * FROM `district` WHERE `_province_id` =:provinceId",
      nativeQuery = true
   )
   List<District> getListDistrictsByProvinceId(@Param("provinceId") int provinceId);
}
