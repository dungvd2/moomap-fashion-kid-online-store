// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.FilterCondition;
import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import com.devcamp.moomapstorebackend.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProductController {
   @Autowired
   ProductService productService;

   @Autowired 
   ProductRepository iProduct;

   public ProductController() {
   }

   @GetMapping({"/products/all"})
   public ResponseEntity<List<Product>> getAllProducts() {
      try {
         List<Product> allProduct = productService.getAllProduct();
         return new ResponseEntity<>(allProduct, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
   
   // API lấy ra thông tin sản phẩm dựa vào tên
   @GetMapping("/products/detail")
   public ResponseEntity<Product> getProductByProductName(@RequestParam("name") String productName) {
      try {
         Product product = iProduct.findByProductName(productName);
         return new ResponseEntity<>(product, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // Hàm lọc sản phẩm có phân trang ở customerPage
   @PostMapping({"/products/customer-page/filter"})
   public ResponseEntity<Page<Product>> filterProductByPage(@RequestBody FilterCondition condition, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "9") int size) {
      try {
         Page<Product> pageNavigation = productService.filterProductByPage(condition, page, size);
         return new ResponseEntity<>(pageNavigation, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }


   // Hàm lọc sản phẩm có phân trang ở admin page
   @GetMapping({"/products"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Page<Product>> getAllProductByaPages( @RequestParam(defaultValue = "") String name, 
                                                               @RequestParam(defaultValue = "") String code, 
                                                               @RequestParam(defaultValue = "0") int page, 
                                                               @RequestParam(defaultValue = "10") int size) {
      try {
         Page<Product> pageNavigation = productService.getProductByPage(code, name, page, size);
         return new ResponseEntity<>(pageNavigation, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/products/{productId}"})
   public ResponseEntity<Product> getProductById(@PathVariable("productId") Integer productId) {
      try {
         Product product = this.productService.getProductById(productId);
         return new ResponseEntity<>(product, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

      }
   }

   @GetMapping({"/product-lines/{productLineId}/products"})
   public ResponseEntity<List<Product>> getAllProductsOfProductLine(@PathVariable("productLineId") Integer productLineId) {
      try {
         List<Product> allProducts = this.productService.getAllProductsOfProductLine(productLineId);
         return new ResponseEntity<>(allProducts, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

      }
   }

   @PostMapping({"product-lines/{productLineId}/products"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Product> createNewProduct(@RequestBody Product pProduct, @PathVariable("productLineId") Integer productLineId) {
      try {
         Product product = this.productService.createProduct(pProduct, productLineId);
         return new ResponseEntity<>(product, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

      }
   }

   @PutMapping({"product-lines/{productLineId}/products/{productId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Product> updateProduct(@RequestBody Product pProduct, @PathVariable("productLineId") Integer productLineId, @PathVariable("productId") Integer productId) {
      try {
         Product product = this.productService.updateProduct(pProduct, productLineId, productId);
         return new ResponseEntity<>(product, HttpStatus.OK);
      } catch (Exception var5) {
         System.out.println(var5);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

      }
   }

   @DeleteMapping({"products/{productId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Order> deleteProduct(@PathVariable("productId") Integer productId) {
      try {
         this.productService.deleteProduct(productId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // Hàm lấy ra danh sách các sản phẩm bán chạy
   @GetMapping("products/popular")
   public ResponseEntity<List<Product>> getListPopularProductsEntity() {
      try {
         List<Product> listProducts = iProduct.getListPopularProduct();
         return new ResponseEntity<>(listProducts, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
