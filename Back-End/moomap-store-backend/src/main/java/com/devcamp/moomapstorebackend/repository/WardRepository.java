// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Ward;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WardRepository extends JpaRepository<Ward, Integer> {
   @Query(
      value = "SELECT * FROM `ward` WHERE `_district_id` =:districtId",
      nativeQuery = true
   )
   List<Ward> getAllWardByDistrictId(@Param("districtId") int districtId);
}
