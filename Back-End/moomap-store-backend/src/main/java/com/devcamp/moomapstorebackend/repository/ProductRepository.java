// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.Product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    // Lấy ra danh sách sản phẩm phổ biến, top 8 bán chạy
    @Query(value = "SELECT p.*, COALESCE(totals.total_product_sold, 0) AS total_product_sold FROM products p LEFT JOIN ( SELECT ps.product_id, COALESCE(SUM(ps.product_sold), 0) AS total_product_sold FROM product_sizes ps GROUP BY ps.product_id ) AS totals ON p.id = totals.product_id ORDER BY totals.total_product_sold DESC LIMIT 8;", nativeQuery = true)
    List<Product> getListPopularProduct();

    // Lấy ra danh sách sản phẩm thông qua giới tính
    @Query(value = "SELECT * FROM products WHERE use_for = :useFor", nativeQuery = true)
    List<Product> getListProductsByGender(@Param("useFor") String useFor);

    // Tìm sản phẩm theo tên, theo mã sản phẩm
    @Query(value = "SELECT * FROM products WHERE product_name LIKE %:name% AND product_code LIKE %:code%", nativeQuery = true)
    List<Product> getListProductByProductNameAndProductCode(@Param("name") String name, @Param("code") String code);

    // Tìm sản phẩm thông qua tên giống
    Product findByProductName(String productName);
}
