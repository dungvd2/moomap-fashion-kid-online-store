// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.models.ProductImage;
import com.devcamp.moomapstorebackend.repository.ProductImageRepository;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductImageService {
   @Autowired
   ProductImageRepository iProductImage;
   @Autowired
   ProductRepository iProduct;

   public ProductImageService() {
   }

   public List<ProductImage> getAllImageOfProduct(Integer productId) {
      return this.iProductImage.findAllByProductId(productId);
   }

   public ProductImage createNewProductImage(Integer productId, ProductImage productImage) {
      Product product = (Product)this.iProduct.findById(productId).get();
      productImage.setProduct(product);
      ProductImage productImageCreated = (ProductImage)this.iProductImage.save(productImage);
      return productImageCreated;
   }

   public void deleteProductImage(Integer productImageId) {
      this.iProductImage.deleteById(productImageId);
   }
}
