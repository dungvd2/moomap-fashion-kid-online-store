// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.District;
import com.devcamp.moomapstorebackend.models.Province;
import com.devcamp.moomapstorebackend.repository.DistrictRepository;
import com.devcamp.moomapstorebackend.repository.ProvinceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistrictSerivce {
   @Autowired
   DistrictRepository iDistrict;
   @Autowired
   ProvinceRepository iProvince;

   public DistrictSerivce() {
   }

   public List<District> getAll() {
      return this.iDistrict.findAll();
   }

   public District createDistrict(District pDistrict, Integer provinceId) {
      Province vProvince = (Province)this.iProvince.findById(provinceId).get();
      pDistrict.setProvince(vProvince);
      District districtCreated = (District)this.iDistrict.save(pDistrict);
      return districtCreated;
   }

   public District updateDistrict(District pDistrict, Integer provinceId, Integer districtId) {
      Province vProvince = (Province)this.iProvince.findById(provinceId).get();
      District vDistrict = (District)this.iDistrict.findById(districtId).get();
      vDistrict.setProvince(vProvince);
      vDistrict.setName(pDistrict.getName());
      vDistrict.setPrefix(pDistrict.getPrefix());
      District updatedDistrict = (District)this.iDistrict.save(vDistrict);
      return updatedDistrict;
   }

   public void deleteDistrict(Integer districtId) {
      this.iDistrict.deleteById(districtId);
   }
}
