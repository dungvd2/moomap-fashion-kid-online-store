// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(
   name = "district"
)
public class District {
   @Id
   @GeneratedValue(
      strategy = GenerationType.IDENTITY
   )
   private int id;
   @Column(
      name = "_name"
   )
   private String name;
   @Column(
      name = "_prefix"
   )
   private String prefix;
   @OneToMany(
      cascade = {CascadeType.ALL},
      mappedBy = "district"
   )
   @JsonIgnore
   private List<Ward> wards;
   @ManyToOne
   @JoinColumn(
      name = "_province_id"
   )
   @JsonIgnore
   private Province province;
   @Transient
   private String provinceName;

   public District() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPrefix() {
      return this.prefix;
   }

   public void setPrefix(String prefix) {
      this.prefix = prefix;
   }

   public List<Ward> getWards() {
      return this.wards;
   }

   public void setWards(List<Ward> wards) {
      this.wards = wards;
   }

   public Province getProvince() {
      return this.province;
   }

   public void setProvince(Province province) {
      this.province = province;
   }

   public int getProvinceId() {
      return this.province.getId();
   }

   public String getProvinceName() {
      return this.province.getName() + ", " + this.province.getCode();
   }
}
