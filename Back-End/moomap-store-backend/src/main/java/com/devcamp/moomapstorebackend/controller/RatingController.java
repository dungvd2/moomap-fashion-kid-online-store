// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.models.Rating;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import com.devcamp.moomapstorebackend.repository.RatingRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;
import com.devcamp.moomapstorebackend.security.UserPrincipal;
import com.devcamp.moomapstorebackend.service.RatingService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RatingController {
   @Autowired
   RatingService ratingService;

   @Autowired
   UserRepository iUser;

   @Autowired
   RatingRepository iRating;

   @Autowired
   ProductRepository iProduct;

   public RatingController() {
   }

   @GetMapping({"/products/{id}/ratings"})
   public ResponseEntity<List<Rating>> getAllRatingOfProduct(@PathVariable("productId") Integer productId) {
      try {
         List<Rating> listRatings = this.ratingService.getAllRatingOfProduct(productId);
         return new ResponseEntity<>(listRatings, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }  

   // Hàm gọi API để lấy ra đánh giá bằng ID
   @GetMapping("/ratings/{ratingId}")
   public ResponseEntity<Rating> getRatingById(@PathVariable("ratingId") Integer ratingId) {
      try {
         Rating rating = iRating.findById(ratingId).get();
         return new ResponseEntity<>(rating, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
   }
   }

   // Hàm gọi API để lấy ra tất cả đánh giá
   @GetMapping("/ratings")
   public ResponseEntity<List<Rating>> getAllRating() {
      try {
         List<Rating> listRatings = iRating.findAll();
         return new ResponseEntity<>(listRatings, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
   }
   }

   // Hàm gọi API để lấy ra đánh giá thông qua tên sản phẩm và người dùng
   @GetMapping("/ratings/find")
   public ResponseEntity<Rating> findRatingByProductNameAndCustomer(@RequestParam(value = "product-name") String productName) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Rating rating = ratingService.findRating(productName, customer);
         return new ResponseEntity<>(rating, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
   }
   }  

   // API để lấy ra tất cả đánh giá có thể hiển thị trên trang customer
   @GetMapping("/ratings/display")
   public ResponseEntity<List<Rating>> getAllRatingCanDisplay(@RequestParam(value = "product-name") String productName) {
      try {
         List<Rating> listRatingsDiplay = ratingService.getRatingCanDisplay(productName);
         return new ResponseEntity<>(listRatingsDiplay, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API để lọc khách hàng theo trạng thái, theo số sao
   @GetMapping("/ratings/filter")
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Page<Rating>> filterRating(
         @RequestParam(defaultValue = "", value = "product-name") String productName,
         @RequestParam(defaultValue = "") Float point,
         @RequestParam(defaultValue = "") String status,
         @RequestParam(defaultValue = "0") int page,
         @RequestParam(defaultValue = "10") int size) {
      try {
         Page<Rating> pageRating = ratingService.filterRatingByPage(productName, status, point, page, size);
         return new ResponseEntity<>(pageRating, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
   
   // API tạo đánh giá cho người dùng
   @PostMapping({"/ratings"})
   public ResponseEntity<Rating> createRating(@RequestBody Rating pRating, @RequestParam(value = "product-name") String productName) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Rating ratingCreated = ratingService.createRatingForProduct(pRating, productName, customer);
         return new ResponseEntity<>(ratingCreated, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API để cập nhật lại đánh giá cho người dùng
   @PutMapping("/ratings/update")
   public ResponseEntity<Rating> updateRating(@RequestBody Rating pRating, @RequestParam(value = "product-name") String productName) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Rating ratingUpdated = ratingService.updateRating(pRating, productName, customer);
         return new ResponseEntity<>(ratingUpdated, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API để cập nhật trạng thái của đánh giá, có cho phép hiển thị hay không
   @PutMapping("/admin/rating/{ratingId}")
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Rating> updateRatingForAdmin(@PathVariable("ratingId") Integer ratingId,@RequestParam String status) {
      try {
         Rating ratingUpdated = ratingService.updateRatingForAdmin(ratingId, status);
         return new ResponseEntity<>(ratingUpdated, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"/ratings/{id}"})
   public ResponseEntity<Rating> deleteRating(@PathVariable("id") Integer id) {
      try {
         this.ratingService.deleteRating(id);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // API kiểm tra xem người đùng đã mua và nếu mua thành công rồi thì đã đánh giá sản phẩm chưa
   @GetMapping({"/users/rating"})
   public ResponseEntity<String> isUserCanRateProduct(@RequestParam(value = "product-name") String productName) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         // Kiểm tra xem đã mua thành công sản phẩm chưa
         Boolean isBoughtProduct = ratingService.isUserBoughtProduct(customer, productName);
         // Kiểm tra xem đã đánh giá sản phẩm chưa
         Boolean isRatedProduct = false;

         Product product = iProduct.findByProductName(productName);
         Rating rating = iRating.findByProductIdAndCustomerId(product.getId(), customer.getId());
         if(rating != null) {
            isRatedProduct = true;
         }

         if(isBoughtProduct && !isRatedProduct) {
            // Nếu mua rồi mà chưa đánh giá thì trả về rate
            return ResponseEntity.ok("rate");
         }
         else if(isBoughtProduct && isRatedProduct && rating.getStatus().equals("approved")){
            // Nếu mua rồi và đánh giá rồi, được phê duyệt rồi thì trả về modify tức là có thể điều chỉnh
            return ResponseEntity.ok("modify");
         }
         else {
            // các trường hợp còn lại thì trả về can't tức là không thể
            return ResponseEntity.ok("can't");
         }
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
