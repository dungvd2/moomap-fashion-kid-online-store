// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

@Entity
@Table(name = "products")
public class Product {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   
   @Column(name = "product_code", unique = true)
   private @NotEmpty(message = "Input product code") String productCode;
   
   @Column(name = "product_name")
   private @NotEmpty(message = "Input product name") String productName;
   
   @Column(name = "product_description")
   private String productDescription;

   @Column(name = "use_for")
   private char userFor;
   
   @Transient
   private String sizes;
   
   @Transient
   private Integer minPrice;
   
   @Transient
   private String productline;

   @Transient
   private Integer productLineId;
   
   @Transient 
   private Integer allProductSold;

   @OneToMany(mappedBy = "product", cascade = {CascadeType.ALL})
   private List<Rating> ratings;
   
   @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "product", fetch = FetchType.LAZY)
   private List<ProductSize> productSizes;
   
   @ManyToOne
   @JoinColumn(name = "product_line_id")
   @JsonIgnore
   private ProductLine productLine;
   
   @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "product")
   private List<ProductImage> productImages;

   public Product() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getProductCode() {
      return this.productCode;
   }

   public void setProductCode(String productCode) {
      this.productCode = productCode;
   }

   public String getProductName() {
      return this.productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public String getProductDescription() {
      return this.productDescription;
   }

   public void setProductDescription(String productDescription) {
      this.productDescription = productDescription;
   }

   public char getUserFor() {
      return this.userFor;
   }

   public void setUserFor(char userFor) {
      this.userFor = userFor;
   }

   public List<ProductSize> getProductSizes() {
      return this.productSizes;
   }

   public void setProductSizes(List<ProductSize> productSizes) {
      this.productSizes = productSizes;
   }

   public ProductLine getProductLine() {
      return this.productLine;
   }

   public void setProductLine(ProductLine productLine) {
      this.productLine = productLine;
   }

   public List<ProductImage> getProductImages() {
      return this.productImages;
   }

   public void setProductImages(List<ProductImage> productImages) {
      this.productImages = productImages;
   }

   public List<Rating> getRatings() {
      return this.ratings;
   }

   public void setRatings(List<Rating> ratings) {
      this.ratings = ratings;
   }

   public String getSizes() {
      if (this.productSizes.size() == 0) {
         return "none";
      } else {
         String resultSize = ((ProductSize)this.productSizes.get(0)).getSize();

         for(int i = 1; i < this.productSizes.size(); ++i) {
            resultSize = resultSize + ", " + ((ProductSize)this.productSizes.get(i)).getSize();
         }

         return resultSize;
      }
   }

   public void setSizes(String sizes) {
      this.sizes = sizes;
   }

   public Integer getMinPrice() {
      if (this.productSizes.size() == 0) {
         return 0;
      } else {
         Integer minPrice = ((ProductSize)this.productSizes.get(0)).getPrice();

         for(int i = 1; i < this.productSizes.size(); ++i) {
            if (minPrice > ((ProductSize)this.productSizes.get(i)).getPrice()) {
               minPrice = ((ProductSize)this.productSizes.get(i)).getPrice();
            }
         }

         return minPrice;
      }
   }

   public void setMinPrice(Integer minPrice) {
      this.minPrice = minPrice;
   }

   public String getProductline() {
      return this.productLine.getProductLine();
   }

   public void setProductline(String productline) {
      this.productline = productline;
   }

   public Integer getProductLineId() {
      return this.productLine.getId();
   }

   public void removeProductSize(List<ProductSize> productSizes) {
      Iterator var3 = productSizes.iterator();

      while(var3.hasNext()) {
         ProductSize size = (ProductSize)var3.next();
         size.setProduct((Product)null);
      }

      this.productSizes = null;
   }

   public void removeProductImages(List<ProductImage> productImages) {
      Iterator var3 = productImages.iterator();

      while(var3.hasNext()) {
         ProductImage image = (ProductImage)var3.next();
         image.setProduct((Product)null);
      }

      this.productImages = null;
   }

   public Integer getAllProductSold() {
      Integer vResult = 0;
      for(ProductSize size : this.productSizes) {
         vResult += size.getProductSold();
      }
      return vResult;
   }

}
